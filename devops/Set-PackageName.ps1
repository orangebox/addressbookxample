param
(
    [string]$configuration,
    [switch]$android,
    [switch]$iOS,
    [switch]$whatIf
)

$scriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path
. $scriptDir\lib\Regex-Functions.ps1

if($Verbose)
{
    $VerbosePreference = "Continue"
}

if($configuration -eq "" -or $configuration -eq $null)
{
	Write-Host "Configuration is required" -ForegroundColor Red
	exit 1
	
}
if(($android -and $iOS) -or (!$android -and !$iOS))
{
    Write-Host "You must specify ONLY android OR iOS - not both, not neither" -ForegroundColor Red
    exit 1
}

# constants
if($configuration -eq "Debug")
{
	$isDebug = $true
	$isRelease = $true
}
elseif($configuration -eq "Release")
{
	$isDebug = $false
	$isRelease = $true
}

$devAppName = "Address Book (Dev)"
$releaseAppName = "Address Book"
$appNameRegex = "Address Book(?: \(Dev\))?"

$androidManifestRelativePath = "../src/Devices/Xample.AddressBook.Android/Properties/AndroidManifest.xml"
$androidDevPackageName = "`${1}`"com.xample.addressbook.dev`""
$androidReleasePackageName = "`${1}`"com.xample.addressbook`""
$androidPackageNameRegex = "(package=)`"com\.xample\.addressbook(?:\.dev)?`""
$androidAppNameRegex = "(<application android:label=)`"$appNameRegex`""

$infoPlistRelativePath = "../src/Devices/Xample.AddressBook.iOS/Info.plist"
$iosDevBundleIdentifier = "`${1}com.xample.addressbook.dev`${2}"
$iosReleaseBundleIdentifier = "`${1}com.xample.addressbook`${2}"
$iosBundleIdentifierRegex = "(<key>CFBundleIdentifier<\/key>\s*<string>)com\.xample\.adddressbook(?:\.dev)?(<\/string>)"
$iosBundleDisplayNameRegex = "(<key>CFBundleDisplayName<\/key>\s*<string>)$appNameRegex(<\/string>)"
$iosBundleNameRegex = "(<key>CFBundleName<\/key>\s*<string>)$appNameRegex(<\/string>)"

if($isDebug)
{
    if($android)
    {
        Write-Verbose "Setting dev/debug values for AndroidManifest.xml"
        Replace-InFileViaRegex $androidManifestRelativePath $androidPackageNameRegex $androidDevPackageName -whatIf:$whatIf
        Replace-InFileViaRegex $androidManifestRelativePath $androidAppNameRegex "`${1}`"$devAppName`"" -whatIf:$whatIf
    }
    if($iOS)
    {
        Write-Verbose "Setting dev/debug values for Info.plist"
        Replace-InFileViaRegex $infoPlistRelativePath $iosBundleIdentifierRegex $iosDevBundleIdentifier -whatIf:$whatIf
        Replace-InFileViaRegex $infoPlistRelativePath $iosBundleDisplayNameRegex "`${1}$devAppName`${2}" -whatIf:$whatIf
        Replace-InFileViaRegex $infoPlistRelativePath $iosBundleNameRegex "`${1}$devAppName`${2}" -whatIf:$whatIf
    }
    Write-Host "Updated to dev/debug values" -ForegroundColor Green
}
elseif ($isRelease) 
{
    if($android)
    {
        Write-Verbose "Setting release values for AndroidManifest.xml"
        Replace-InFileViaRegex $androidManifestRelativePath $androidPackageNameRegex $androidReleasePackageName -whatIf:$whatIf
        Replace-InFileViaRegex $androidManifestRelativePath $androidAppNameRegex "`${1}`"$releaseAppName`"" -whatIf:$whatIf
    }
    if($iOS)
    {
        Write-Verbose "Setting release values for Info.plist"
        Replace-InFileViaRegex $infoPlistRelativePath $iosBundleIdentifierRegex $iosReleaseBundleIdentifier -whatIf:$whatIf
        Replace-InFileViaRegex $infoPlistRelativePath $iosBundleDisplayNameRegex "`${1}$releaseAppName`${2}" -whatIf:$whatIf
        Replace-InFileViaRegex $infoPlistRelativePath $iosBundleNameRegex "`${1}$releaseAppName`${2}" -whatIf:$whatIf
    }        
    Write-Host "Updated to release values" -ForegroundColor Green
}
