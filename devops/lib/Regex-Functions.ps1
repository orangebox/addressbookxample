function Replace-InFileViaRegex([string]$filePath, [string]$regexPattern, [string]$replacementText, [switch]$whatIf)
{
    if(!(Test-Path $filePath))
    {
        throw "Unable to locate $filePath"
    }

    Write-Verbose "File: `"$filePath`", Regex: `"$regexPattern`", Replacement: `"$replacementText`""

    $content = (Get-Content $filePath | Out-String).TrimEnd()

    $isMatch = $content -match $regex
    if($isMatch)
    {
        Write-Verbose "`"$filePath`" has at least one match for `"$regexPattern`""
        $updatedContent = $content -replace $regexPattern, $replacementText
        if($whatIf)
        {
            Write-Host $updatedContent
        }
        else 
        {
            $updatedContent | Out-File $filePath -Encoding "utf8"
        }
    }
    else 
    {
        Write-Host "`"$filePath`" does not have a match for `"$regexPattern`"" -ForegroundColor Red
    }
}
