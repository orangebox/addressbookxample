﻿using System;
using Android.Text;
using Xample.Bcl.Android.Renderers;
using Xample.Bcl.Xamarin.Controls;
using Xample.Bcl.Xamarin.Controls.BaseRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;

[assembly: ExportRenderer(typeof(FreeTextEntry), typeof(FreeTextEntryRenderer))]
namespace Xample.Bcl.Android.Renderers
{
    public class FreeTextEntryRenderer : EntryRenderer
    {
        public FreeTextEntryRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            FreeTextEntryRendererBase.OnElementChanged(Control, e.NewElement, UpdateTypingProperties);
        }

        private void UpdateTypingProperties(FreeTextEntry freeTextEntry)
        {
            var spellCheckFlag = SetSpellCheckFlag(freeTextEntry.EnableSpellCheck);
            var autoCorrectFlag = SetAutoCorrectFlag(freeTextEntry.EnableAutoCorrect);
            var autoCapitalizationFlag = SetAutoCapitalizationFlag(freeTextEntry.CapitalizatonType);

            Control.SetRawInputType(spellCheckFlag | autoCorrectFlag | autoCapitalizationFlag);
        }

        private static InputTypes SetSpellCheckFlag(bool enableSpellCheck)
        {
            if (enableSpellCheck)
                return InputTypes.Null;
            else
                return InputTypes.TextFlagNoSuggestions;
        }

        private static InputTypes SetAutoCorrectFlag(bool enableAutoCorrect)
        {
            if (enableAutoCorrect)
                return InputTypes.TextFlagAutoCorrect;
            else
                return InputTypes.Null;
        }

        private static InputTypes SetAutoCapitalizationFlag(AutoCapitalizationType capitalizationType)
        {
            switch (capitalizationType)
            {
                case AutoCapitalizationType.None:
                    return InputTypes.Null;
                case AutoCapitalizationType.Sentences:
                    return InputTypes.TextFlagCapSentences;
                case AutoCapitalizationType.Words:
                    return InputTypes.TextFlagCapWords;
                default:
                    throw new NotSupportedException($"Unhandled capitalization type: {capitalizationType}");
            }
        }

    }
}
