﻿using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xample.Bcl.Android.Renderers;
using Xample.Bcl.Xamarin.Controls;

[assembly: ExportRenderer(typeof(ExpandableLabel), typeof(ExpandableLabelRenderer))]
namespace Xample.Bcl.Android.Renderers
{
    public class ExpandableLabelRenderer : ButtonRenderer
    {
        public ExpandableLabelRenderer(Context context) 
            : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            Control.SetAllCaps(false);
        }

    }
}