﻿using Android.App;
using Xample.Bcl.Xamarin.PlatformServices;
using Xamarin.Forms;

namespace Xample.Bcl.Android.PlatformServices
{
    public class ProcessKiller : IProcessKiller
    {
        public void Kill()
        {
            var activity = (Activity)Forms.Context;
            activity.FinishAffinity();
        }

    }
}
