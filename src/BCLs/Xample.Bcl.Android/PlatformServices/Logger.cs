﻿using System;
using System.Text;
using Xample.Bcl.Extensions;
using Xample.Bcl.Xamarin.PlatformServices;

namespace Xample.Bcl.Android.PlatformServices
{
    public class Logger : ILogger
    {
        private string callingClass = "LOGGER NOT INITIALIZED";

        public void Initialize(Type callingClass)
        {
            this.callingClass = callingClass.Name;
        }

        public void Log(XampleLogLevel level, string message)
        {
            var timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff");
            var logLevel = level.ToString().ToUpper();
            var logMessage = $"{timestamp} {callingClass} - {logLevel}: {message}";

            if (level != XampleLogLevel.Error)
                Console.WriteLine(logMessage);
            else
                Console.Error.WriteLine(logMessage);
        }

        public void Trace(string message)
        {
            Log(XampleLogLevel.Trace, message);
        }

        public void Debug(string message)
        {
            Log(XampleLogLevel.Debug, message);
        }

        public void Info(string message)
        {
            Log(XampleLogLevel.Info, message);
        }

        public void Warning(string message)
        {
            Log(XampleLogLevel.Warning, message);
        }

        public void Error(string message)
        {
            Log(XampleLogLevel.Error, message);
        }

        public void Error(Exception exception)
        {
            Error(String.Empty, exception);
        }

        public void Error(string message, Exception exception)
        {
            var messageBuilder = new StringBuilder(message);
            var exceptions = exception.Flatten();
            var exceptionText = exceptions.ListToString();
            messageBuilder.Append(exceptionText);

            Log(XampleLogLevel.Error, messageBuilder.ToString());
        }

    }
}
