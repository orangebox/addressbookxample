﻿using System;
using System.IO;
using Xample.Bcl.Xamarin.PlatformServices;

namespace Xample.Bcl.Android.PlatformServices
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}
