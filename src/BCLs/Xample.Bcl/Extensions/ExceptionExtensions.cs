﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Xample.Bcl.Extensions
{
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Turn an <see cref="Exception"/>'s InnerExceptions into a collection of <see cref="Exception"/>s
        /// </summary>
        /// <param name="ex"></param>
        /// <returns>A collection of <see cref="Exception"/>s</returns>
        public static List<Exception> Flatten(this Exception ex)
        {
            var exceptionList = new List<Exception>();
            var currentException = ex;
            do
            {
                exceptionList.Add(currentException);
                currentException = currentException.InnerException;
            } while (currentException != null);
            return exceptionList;
        }

        public static void PrintToConsole(this List<Exception> exceptions)
        {
            var message = ListToString(exceptions);
            Debug.WriteLine(message);
        }

        public static string ListToString(this List<Exception> exceptions)
        {
            const string SEPARATOR = "----------";
            var messageBuilder = new StringBuilder();

            messageBuilder.AppendLine($"Exceptions caught: {exceptions.Count.ToString("N0")}");
            for (var index = 0; index < exceptions.Count; index++)
            {
                var exception = exceptions[index];

                messageBuilder.AppendLine($"Exception {index + 1}: {exception.GetType().Name} caught");
                messageBuilder.AppendLine($"Message: {exception.Message}");
                messageBuilder.AppendLine($"Stack Trace:\n{exception.StackTrace}");
                messageBuilder.AppendLine(SEPARATOR);
            }

            return messageBuilder.ToString();
        }

    }
}
