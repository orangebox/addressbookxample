﻿using Xample.Bcl.Xamarin.Mvvm.Navigation;
using Xamarin.Forms;

namespace Xample.Bcl.Extensions
{
    public static class PageExtensions
    {
        public static bool IsModal(this Page page)
        {
            return page is IModalPage;
        }
    }
}
