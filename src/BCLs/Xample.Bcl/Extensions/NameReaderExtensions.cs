﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;

//NOTE: This class was taken from https://www.codeproject.com/Articles/1070785/Get-Property-Names-Using-Lambda-Expressions-in-Csh
namespace Xample.Bcl.Extensions
{
    public static class NameReaderExtensions
    {
        public static string GetMemberName<T>(this Expression<Func<T, Object>> expression)
        {
            return GetMemberName(expression.Body);
        }

        public static IEnumerable<String> GetMemberNames<T>(this T instance, params Expression<Func<T, Object>>[] expressions)
        {
            var memberNames = new List<String>();
            foreach (var cExpression in expressions)
            {
                memberNames.Add(GetMemberName(cExpression.Body));
            }

            return memberNames;
        }

        public static string GetMemberName<T>(this Expression<Func<T>> expression)
        {
            return GetMemberName(expression.Body);
        }

        private static string GetMemberName(this Expression expression)
        {
            if (expression == null)
                throw new ArgumentException("The expression cannot be null.");

            // Reference type property or field
            if (expression is MemberExpression memberExpression)
                return memberExpression.Member.Name;

            // Reference type method
            if (expression is MethodCallExpression methodCallExpression)
                return methodCallExpression.Method.Name;

            // Property, field of method returning value type
            if (expression is UnaryExpression unaryExpression)
                return GetMemberName(unaryExpression);

            throw new ArgumentException("Invalid expression.");
        }

        private static string GetMemberName(this UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression methodExpression)
                return methodExpression.Method.Name;

            return ((MemberExpression)unaryExpression.Operand).Member.Name;
        }
    }
}
