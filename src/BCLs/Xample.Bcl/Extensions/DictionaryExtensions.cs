﻿using System.Collections.Generic;
using System;

namespace Xample.Bcl.Extensions
{
    public static class DictionaryExtensions
    {
        public static void Add<T, U>(this Dictionary<T, U> dictionary, KeyValuePair<T, U> kvp)
        {
            dictionary.Add(kvp.Key, kvp.Value);
        }

        public static void AddRange<T, U>(this IDictionary<T, U> dictionary, IDictionary<T, U> itemsToAdd)
        {
            if (itemsToAdd == null)
                throw new ArgumentNullException(nameof(itemsToAdd), "Dictionary is null");

            foreach (var item in itemsToAdd)
            {
                dictionary.Add(item);
            }
        }

        public static void UpdateRange<T, U>(this IDictionary<T, U> dictionary, IDictionary<T, U> itemsToUpdate)
        {
            if (itemsToUpdate == null)
                throw new ArgumentNullException(nameof(itemsToUpdate), "Dictionary is null");

            foreach (var item in itemsToUpdate)
            {
                if (!dictionary.ContainsKey(item.Key))
                    throw new ArgumentException($"The key {item.Key} does not exist in this dictionary");
                
                dictionary[item.Key] = item.Value;
            }
        }

        public static void RemoveRange<T,U>(this IDictionary<T,U> dictionary, IDictionary<T,U> itemsToRemove)
        {
            if (itemsToRemove == null)
                throw new ArgumentNullException(nameof(itemsToRemove), "Dictionary is null");

            foreach(var item in itemsToRemove)
            {
                if (!dictionary.ContainsKey(item.Key))
                    throw new ArgumentException($"The key {item.Key} does not exist in this dictionary");
                
                dictionary.Remove(item.Key);
            }
        }

    }
}
