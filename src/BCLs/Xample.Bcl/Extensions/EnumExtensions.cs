﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Xample.Bcl.Extensions
{
    public static class EnumExtensions
    {
        public static IList<T> ToList<T>() where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }

        public static TAttribute GetAttribute<TAttribute>(this Enum value)where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetRuntimeField(name)
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();
        }
    }
}
