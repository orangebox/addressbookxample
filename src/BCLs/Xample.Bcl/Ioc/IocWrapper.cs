﻿using System;
using GalaSoft.MvvmLight.Ioc;

namespace Xample.Bcl.Ioc
{
    public static class IocWrapper
    {
        public static void ResetIocContainer()
        {
            SimpleIoc.Default.Reset();
        }

        public static void Register<T>()
            where T : class
        {
            SimpleIoc.Default.Register<T>();
        }

        public static void Register<TInterface, TClass>()
            where TInterface : class
            where TClass : class, TInterface
        {
            SimpleIoc.Default.Register<TInterface, TClass>();
        }

        public static void Register<T>(Func<T> factory)
            where T : class
        {
            SimpleIoc.Default.Register(factory);
        }

        public static T Get<T>()
        {
            return SimpleIoc.Default.GetInstance<T>();
        }

        public static object Get(Type type)
        {
            return SimpleIoc.Default.GetService(type);
        }

        public static TClass Get<TInterface, TClass>()
            where TInterface : class
            where TClass : class
        {
            var svc = SimpleIoc.Default.GetService(typeof(TInterface)) as TClass;
            if (svc == null)
                throw new InvalidCastException($"Unable to cast {typeof(TInterface).Name} to {typeof(TClass).Name}");
            return svc;
        }

    }
}
