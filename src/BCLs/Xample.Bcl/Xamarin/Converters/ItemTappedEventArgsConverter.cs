﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Converters
{
    public class ItemTappedEventArgsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var eventArgs = value as ItemTappedEventArgs;
            if (eventArgs == null)
                throw new ArgumentException($"Expected a value of {typeof(ItemTappedEventArgs).Name}", nameof(value));
            return eventArgs.Item;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
