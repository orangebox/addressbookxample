﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolValue = System.Convert.ToBoolean(value);
            switch (parameter?.ToString() ?? "")
            {
                case "negate":
                case "neg":
                case "n":
                    return !boolValue;
                default:
                    return boolValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isVisible = System.Convert.ToBoolean(value);
            switch (parameter?.ToString() ?? "")
            {
                case "negate":
                case "neg":
                case "n":
                    return !isVisible;
                default:
                    return isVisible;
            }
        }

    }
}
