﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using Xample.Bcl.Ioc;
using Xample.Bcl.Xamarin.PlatformServices;

namespace Xample.Bcl.Xamarin.Localization
{
    public class LanguageManager
    {
        #region Singleton Implementation
        private static Lazy<LanguageManager> lazyManager = new Lazy<LanguageManager>(() => new LanguageManager());
        public static LanguageManager Instance { get { return lazyManager.Value; } }
        #endregion

        #region Variables
        private readonly ILogger logger;
        #endregion

        #region Constructors
        public LanguageManager()
        {
            logger = IocWrapper.Get<ILogger>();
            logger.Initialize(GetType());
        }
        #endregion

        #region Variables
        private bool isInitialized;

        public IAppLanguageManager AppLanguageManager { get; private set; }
        public IList<CultureInfo> SupportedLanguages { get; private set; }
        public ResourceManager ResourceManager { get; private set; }
        public CultureInfo CurrentLanguage
        {
            get
            {
                var lang = IocWrapper.Get<ILocalize>().GetCurrentCulture();
                return SupportedLanguages.FirstOrDefault(ci => ci.TwoLetterISOLanguageName == lang.TwoLetterISOLanguageName);
            }
        }
        #endregion

        #region Public Methods
        public void Initialize(IAppLanguageManager alm)
        {
            if (isInitialized)
                return;

            isInitialized = true;

            AppLanguageManager = alm;

            var path = alm.StringResourcesType.FullName;
            var assembly = alm.StringResourcesType.GetTypeInfo().Assembly;
            ResourceManager = new ResourceManager(path, assembly);

            SupportedLanguages = new List<CultureInfo>();
            foreach (var code in alm.LanguageCodes)
            {
                logger.Info($"Adding language: {code}");
                SupportedLanguages.Add(new CultureInfo(code));
            }
        }
        #endregion
    }
}
