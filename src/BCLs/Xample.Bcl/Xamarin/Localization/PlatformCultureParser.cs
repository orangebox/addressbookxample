﻿using System;

namespace Xample.Bcl.Xamarin.Localization
{
    /// <summary>
    /// Helper class for splitting locales like
    ///   iOS: ms_MY, gsw_CH
    ///   Android: in-ID
    /// into parts so we can create a .NET culture (or fallback culture)
    /// </summary>
    public static class PlatformCultureParser
    {
        public static PlatformCulture Parse(string platformCultureString)
        {
            if (String.IsNullOrEmpty(platformCultureString))
                throw new ArgumentException("Expected culture identifier", nameof(platformCultureString));

            var platformString = platformCultureString.Replace("_", "-");
            var dashIndex = platformString.IndexOf("-", StringComparison.Ordinal);
            if (dashIndex > 0)
            {
                var parts = platformString.Split('-');
                var languageCode = parts[0];
                var localeCode = parts[1];
                return new PlatformCulture(platformString, languageCode, localeCode);
            }

            return new PlatformCulture(platformString, platformString, String.Empty);

        }

    }
}
