﻿namespace Xample.Bcl.Xamarin.Localization
{
    public class PlatformCulture
    {
        public string PlatformString { get; }
        public string LanguageCode { get; }
        public string LocaleCode { get; }

        public PlatformCulture(string plat, string lang, string loc)
        {
            PlatformString = plat;
            LanguageCode = lang;
            LocaleCode = loc;
        }

        public override string ToString()
        {
            return PlatformString;
        }

    }
}
