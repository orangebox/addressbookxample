﻿using System;
using System.Collections.Generic;

namespace Xample.Bcl.Xamarin.Localization
{
    public interface IAppLanguageManager
    {
        Type StringResourcesType { get; }
        IEnumerable<String> LanguageCodes { get; }
    }
}
