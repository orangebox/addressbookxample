﻿using System;
using System.Globalization;
using Xample.Bcl.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xample.Bcl.Xamarin.Localization
{
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
		#region Variables
        private readonly CultureInfo culture;

        public string Text { get; set; }
        #endregion

        #region Constructors
        public TranslateExtension()
        {
            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                culture = IocWrapper.Get<ILocalize>().GetCurrentCulture();
            }
        }
        #endregion

        #region Public methods
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return String.Empty;

            var translation = LanguageManager.Instance.ResourceManager.GetString(Text, culture);
            if (translation == null)
            {
#if DEBUG
                throw new ArgumentException($"Key '{Text}' was not found in the string resources file for culture '{culture.Name}'.", nameof(Text));
#else
                // HACK: returns the key, which GETS DISPLAYED TO THE USER IN THE UI
				translation = Text; 
#endif
            }
            return translation;
        }
        #endregion

    }
}
