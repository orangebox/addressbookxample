﻿using System;

namespace Xample.Bcl.Xamarin.AdMob
{
    public static class AdsContext
    {
        private const string DEFAULT_ID = "Unset";

        private static string bannerId = DEFAULT_ID;
        public static string BannerId
        {
            get
            {
                if (bannerId == DEFAULT_ID)
                    throw new ArgumentException("You forgot to set the banner ID", nameof(bannerId));
                return bannerId;
            }
            set
            {
                bannerId = value;
            }
        }

        private static string deviceId = DEFAULT_ID;
        public static string DeviceId
        {
            get
            {
                if (deviceId == DEFAULT_ID)
                    throw new ArgumentException("You forgot to set the device ID", nameof(deviceId));
                return deviceId;
            }
            set
            {
                deviceId = value;
            }
        }

    }
}
