﻿using System;
using System.Reflection;
using System.Windows.Input;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Behaviors
{
    /// <summary>
    /// Taken and adapted from:
    /// https://github.com/xamarin/xamarin-forms-samples/blob/master/Behaviors/EventToCommandBehavior/EventToCommandBehavior/Behaviors/EventToCommandBehavior.cs
    /// </summary>
    public class EventToCommand : BehaviorBase<View>
    {
        #region Properties
        private Delegate eventHandler;

        public static readonly BindableProperty EventNameProperty = CreateBindableProperty(nameof(EventName), typeof(String), OnEventNameChanged);
        public static readonly BindableProperty CommandProperty = CreateBindableProperty(nameof(Command), typeof(ICommand));
        public static readonly BindableProperty CommandParameterProperty = CreateBindableProperty(nameof(CommandParameter), typeof(Object));
        public static readonly BindableProperty InputConverterProperty = CreateBindableProperty(nameof(Converter), typeof(IValueConverter));

        public string EventName
        {
            get { return (String)GetValue(EventNameProperty); }
            set { SetValue(EventNameProperty, value); }
        }
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
        public IValueConverter Converter
        {
            get { return (IValueConverter)GetValue(InputConverterProperty); }
            set { SetValue(InputConverterProperty, value); }
        }
        #endregion

        #region Constructors
        #endregion

        #region Private Methods
        private void RegisterEvent(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
                return;

            var eventInfo = AssociatedObject.GetType().GetRuntimeEvent(name);
            if (eventInfo == null)
                throw new ArgumentException($"{GetType().Name}: Can't register the '{EventName}' event.");

            var methodInfo = typeof(EventToCommand).GetTypeInfo().GetDeclaredMethod("OnEvent");
            eventHandler = methodInfo.CreateDelegate(eventInfo.EventHandlerType, this);
            eventInfo.AddEventHandler(AssociatedObject, eventHandler);
        }

        private void UnregisterEvent(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
                return;
            if (eventHandler == null)
                return;

            var eventInfo = AssociatedObject.GetType().GetRuntimeEvent(name);
            if (eventInfo == null)
                throw new ArgumentException($"{GetType().Name}: Can't unregister from the '{EventName}' event.");

            eventInfo.RemoveEventHandler(AssociatedObject, eventHandler);
            eventHandler = null;
        }

        private void OnEvent(object sender, object eventArgs)
        {
            if (Command == null)
                return;

            object resolvedParameter;
            if (CommandParameter != null)
                resolvedParameter = CommandParameter;
            else if (Converter != null)
                resolvedParameter = Converter.Convert(eventArgs, typeof(Object), null, null);
            else
                resolvedParameter = eventArgs;

            if (Command.CanExecute(resolvedParameter))
                Command.Execute(resolvedParameter);
        }

        private static BindableProperty CreateBindableProperty(string propertyName, Type propertyType)
        {
            return BindableProperty.Create(propertyName, propertyType, typeof(EventToCommand), null);
        }

        private static BindableProperty CreateBindableProperty(string propertyName, Type propertyType, BindableProperty.BindingPropertyChangedDelegate @delegate)
        {
            return BindableProperty.Create(propertyName, propertyType, typeof(EventToCommand), null, propertyChanged: @delegate);
        }

        private static void OnEventNameChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var behavior = (EventToCommand)bindable;
            if (behavior.AssociatedObject == null)
                return;

            var oldEventName = (String)oldValue;
            var newEventName = (String)newValue;

            behavior.UnregisterEvent(oldEventName);
            behavior.RegisterEvent(newEventName);
        }

        protected override void OnAttachedTo(View bindable)
        {
            base.OnAttachedTo(bindable);
            RegisterEvent(EventName);
        }

        protected override void OnDetachingFrom(View bindable)
        {
            UnregisterEvent(EventName);
            base.OnDetachingFrom(bindable);
        }
        #endregion

    }
}
