﻿using System;
using System.Threading.Tasks;

namespace Xample.Bcl.Xamarin.Mvvm.Dialog
{
    public interface IDialogService
    {
        bool IsInitialized { get; }

        Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback = null);
        Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback = null);
        Task<bool> ShowMessage(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback = null);

        Task<String> ShowActionSheet(string title, Action<String> afterHideCallback, params string[] buttons);
        Task<String> ShowActionSheet(string title, string buttonCancelText, Action<String> afterHideCallback, params string[] buttons);
        Task<String> ShowActionSheet(string title, string buttonCancelText, string buttonDestructionText, Action<String> afterHideCallback, params string[] buttons);
    }
}
