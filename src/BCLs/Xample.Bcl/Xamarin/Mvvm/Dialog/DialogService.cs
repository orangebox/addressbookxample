﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Mvvm.Dialog
{
    public class DialogService : IDialogService
    {
        private Page _dialogPage;
        public bool IsInitialized { get; private set; }

        public void Initialize(Page dialogPage)
        {
            _dialogPage = dialogPage;
            IsInitialized = true;
        }

        public async Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback = null)
        {
            await ShowMessage(error.Message, title, buttonText, afterHideCallback);
        }

        public async Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback = null)
        {
            await _dialogPage.DisplayAlert(title, message, buttonText);
            afterHideCallback?.Invoke();
        }

        public async Task<bool> ShowMessage(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback = null)
        {
            var result = await _dialogPage.DisplayAlert(title, message, buttonConfirmText, buttonCancelText);
            afterHideCallback?.Invoke(result);
            return result;
        }

        public async Task<String> ShowActionSheet(string title, Action<String> afterHideCallback, params string[] buttons)
        {
            return await ShowActionSheet(title, null, null, afterHideCallback, buttons);
        }

        public async Task<String> ShowActionSheet(string title, string buttonCancelText, Action<String> afterHideCallback, params string[] buttons)
        {
            return await ShowActionSheet(title, buttonCancelText, null, afterHideCallback, buttons);
        }

        public async Task<String> ShowActionSheet(string title, string buttonCancelText, string buttonDestructionText, Action<String> afterHideCallback, params string[] buttons)
        {
            var result = await _dialogPage.DisplayActionSheet(title, buttonCancelText, buttonDestructionText, buttons);
            afterHideCallback?.Invoke(result);
            return result;
        }

    }
}
