﻿using System;
using System.Threading.Tasks;

namespace Xample.Bcl.Xamarin.Mvvm.Dialog
{
    public class DesignDialogService : IDialogService
    {
        public bool IsInitialized { get; set; }

        public Task<string> ShowActionSheet(string title, Action<string> afterHideCallback, params string[] buttons)
        {
            throw new NotImplementedException();
        }

        public Task<string> ShowActionSheet(string title, string buttonCancelText, Action<string> afterHideCallback, params string[] buttons)
        {
            throw new NotImplementedException();
        }

        public Task<string> ShowActionSheet(string title, string buttonCancelText, string buttonDestructionText, Action<string> afterHideCallback, params string[] buttons)
        {
            throw new NotImplementedException();
        }

        public Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback = null)
        {
            throw new NotImplementedException();
        }

        public Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ShowMessage(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback = null)
        {
            throw new NotImplementedException();
        }
    }
}
