﻿using GalaSoft.MvvmLight;

namespace Xample.Bcl.Xamarin.Mvvm.ViewModels
{
    public abstract class DesignableViewModelBase : ViewModelBase
    {
        protected abstract void SetDesignTimeData();
    }
}
