﻿namespace Xample.Bcl.Xamarin.Mvvm.Navigation
{
    public interface INavigationService
    {
        bool IsInitialized { get; }

        string CurrentPageKey { get; }

        void GoBack();

        void NavigateTo(string pageKey);
        void NavigateTo(string pageKey, object parameter);
    }
}
