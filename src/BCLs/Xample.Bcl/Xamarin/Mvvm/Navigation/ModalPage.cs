﻿using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Mvvm.Navigation
{
    public class ModalPage : ContentPage, IModalPage
    {
        public ModalPage()
        {
            if (Device.RuntimePlatform != Device.iOS)
                return;

            Padding = new Thickness(this.Padding.Left, this.Padding.Top + 20.0, this.Padding.Right, this.Padding.Bottom);
        }
    }
}
