﻿
namespace Xample.Bcl.Xamarin.Mvvm.Navigation
{
    public class DesignNavigationService : INavigationService
    {
        public bool IsInitialized { get; }
        public string CurrentPageKey { get; private set; }

        public void GoBack() { }
        public void NavigateTo(string pageKey) { }
        public void NavigateTo(string pageKey, object parameter) { }
    }
}
