﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xample.Bcl.Extensions;

namespace Xample.Bcl.Xamarin.Mvvm.Navigation
{
    public class NavigationService : INavigationService, INotifyPropertyChanged
    {
        #region Variables
        private readonly Dictionary<String, Type> pagesByKey;
        private NavigationPage navigationPage;

        public bool IsInitialized { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Binding variables
        private string currentPageKey;
        public string CurrentPageKey
        {
            get { return currentPageKey; }
            set
            {
                if (currentPageKey == value)
                    return;
                currentPageKey = value;
                RaisePropertyChanged(nameof(CurrentPageKey));
            }
        }
        #endregion

        #region Constuctor
        public NavigationService()
        {
            pagesByKey = new Dictionary<string, Type>();
        }

        ~NavigationService()
        {
            navigationPage.Pushed -= OnPageChange;
            navigationPage.Popped -= OnPageChange;
        }
        #endregion

        #region Public Navigation Methods
        public void GoBack()
        {
            if (navigationPage.Navigation.ModalStack.Any())
                navigationPage.Navigation.PopModalAsync();
            else
                navigationPage.PopAsync();
        }
        
        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null);
        }
		
		public void NavigateTo(string pageKey, object parameter)
		{
            lock (pagesByKey)
            {
                if (!pagesByKey.ContainsKey(pageKey))
                    throw new ArgumentException($"No such page \"{pageKey}\". Did you forget to call NavigationService.Configure()?");

                try
                {
                    var page = InstantiatePage(pageKey, parameter);
                    DisplayPageAsync(page);
                }
                catch(Exception ex)
                {
                    throw new Exception($"Unable to instantiate page {pageKey}.", ex);
                }
            }
        }
        #endregion

        #region Public Initialization Methods
        public void Initialize(NavigationPage navigation)
		{
			navigationPage = navigation;
			navigationPage.Pushed += OnPageChange;
			navigationPage.Popped += OnPageChange;
			UpdateCurrentPageKey();
			IsInitialized = true;
		}
		
        public void Configure(Type viewmodelType, Type pageType)
        {
            Configure(viewmodelType.Name, pageType);
        }

        public void Configure(string pageKey, Type pageType)
        {
            lock (pagesByKey)
            {
                if (pagesByKey.ContainsKey(pageKey) == true)
                    pagesByKey[pageKey] = pageType;
                else
                    pagesByKey.Add(pageKey, pageType);
            }
        }

        public bool ContainsKey(string pageKey)
        {
            return pagesByKey.ContainsKey(pageKey);
        }
        #endregion

        #region Private Methods
        private object[] BuildParameters(object parameter)
        {
            if (parameter != null)
                return new object[] { parameter };
            return null;
        }

        private Page InstantiatePage(string pageKey, object parameter)
        {
            var pageType = pagesByKey[pageKey];
            var parameters = BuildParameters(parameter);

            var @object = Activator.CreateInstance(pageType, parameters);
            var page = @object as Page;
            return page;
        }

        private Task DisplayPageAsync(Page page)
        {
            if (page.IsModal())
                return navigationPage.Navigation.PushModalAsync(page);
            
            return navigationPage.PushAsync(page);
        }
        #endregion

        #region Event Handlers
        private void OnPageChange(object sender, NavigationEventArgs e)
        {
            UpdateCurrentPageKey();
        }
        #endregion

        #region INotifyPropertyChanged Implementation
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UpdateCurrentPageKey()
        {
            lock (pagesByKey)
            {
                if (navigationPage?.CurrentPage == null)
                    return;
                var pageType = navigationPage.CurrentPage.GetType();
                var pageKey = pagesByKey.ContainsValue(pageType) ? pagesByKey.First(p => p.Value == pageType).Key : null;
                CurrentPageKey = pageKey;
            }
        }
        #endregion

    }
}
