﻿using System;
using System.ComponentModel;
using Xample.Bcl.Text;
using Xample.Bcl.Xamarin.Localization;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Controls
{
    public class FormattedNumericEntry : Entry
    {
        #region Bindable Properties
        public static readonly BindableProperty StringFormatProperty = CreateBindableProperty<String>(nameof(StringFormat));
        #endregion

        #region Variables
        private readonly NumericStringFormatter numericStringFormatter;
        private bool isEditing;

        public string StringFormat
        {
            get { return (String)GetValue(StringFormatProperty); }
            set { SetValue(StringFormatProperty, value); }
        }
        #endregion

        #region Constructors
        public FormattedNumericEntry()
        {
            numericStringFormatter = new NumericStringFormatter(LanguageManager.Instance.CurrentLanguage);

            isEditing = false;

            Keyboard = Keyboard.Numeric;
            Focused += OnFocus;
            Unfocused += OnUnfocus;
            PropertyChanged += OnPropertyChanged;
        }
        #endregion

        #region Private Methods
        private static BindableProperty CreateBindableProperty<T>(string propertyName)
        {
            return CreateBindableProperty<T>(propertyName, default(T));
        }

        private static BindableProperty CreateBindableProperty<T>(string propertyName, T defaultValue)
        {
            return BindableProperty.Create(propertyName: propertyName,
                                           returnType: typeof(T),
                                           declaringType: typeof(FormattedNumericEntry),
                                           defaultValue: defaultValue);
        }

        private void ApplyFormatting()
        {
            var formattedText = numericStringFormatter.ApplyFormatting(Text, StringFormat);
            Text = formattedText;
        }

        private void RemoveFormatting()
        {
            var unformattedText = numericStringFormatter.RemoveFormatting(Text);
            Text = unformattedText;
        }
        #endregion

        #region Event Handlers
        private void OnFocus(object sender, EventArgs e)
        {
            isEditing = true;
            RemoveFormatting();
        }

        private void OnUnfocus(object sender, EventArgs e)
        {
            isEditing = false;
            ApplyFormatting();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(Text))
                return;
            if (IsFocused || isEditing)
                return;

            ApplyFormatting();
        }
        #endregion
    }
}
