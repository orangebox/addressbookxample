﻿using System;

namespace Xample.Bcl.Xamarin.Controls.BaseRenderers
{
    public static class FreeTextEntryRendererBase
    {
        public static void OnElementChanged(object control, object newElement, Action<FreeTextEntry> updateTypingPropertiesCallback)
        {
            if (!AreElementsValid(control, newElement))
                return;

            var freeTextEntry = newElement as FreeTextEntry;

            freeTextEntry.PropertyChanged += (s, e) =>
            {
                var sender = s as FreeTextEntry;
                var propertyName = e.PropertyName;

                OnPropertyChanged(sender, propertyName, updateTypingPropertiesCallback);
            };
            updateTypingPropertiesCallback.Invoke(freeTextEntry);
        }

        private static bool AreElementsValid(object control, object newElement)
        {
            return control != null &&
                   newElement != null &&
                   newElement is FreeTextEntry;
        }

        private static void OnPropertyChanged(FreeTextEntry sender, string propertyName, Action<FreeTextEntry> updateTypingPropertiesCallback)
        {
            if (sender == null)
                return;

            if (!FreeTextEntry.IsTypingPropertyName(propertyName))
                return;

            updateTypingPropertiesCallback.Invoke(sender);
        }

    }
}
