﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Xample.Bcl.Xamarin.Controls
{
    public class ExpandableLabel : Button
    {
        #region Bindable Properties
        public static readonly BindableProperty ShortTextProperty = CreateBindableProperty<String>(nameof(ShortText));
        public static readonly BindableProperty ExpandedTextProperty = CreateBindableProperty<String>(nameof(ExpandedText));
        public static readonly BindableProperty IsExpandableProperty = CreateBindableProperty<Boolean>(nameof(IsExpandable));
        #endregion

        #region Variables
        private const int ANIMATION_DURATION = 125;
        private const int EXPANDED_DURATION = 5000;
        private bool isExpanded;

        public string ShortText
        {
            get { return (String)GetValue(ShortTextProperty); }
            set { SetValue(ShortTextProperty, value); }
        }
        public string ExpandedText
        {
            get { return (String)GetValue(ExpandedTextProperty); }
            set { SetValue(ExpandedTextProperty, value); }
        }
        public bool IsExpandable
        {
            get { return (Boolean)GetValue(IsExpandableProperty); }
            set { SetValue(IsExpandableProperty, value); }
        }
        public new string Text
        {
            get { return base.Text; }
            private set { base.Text = value; }
        }
        #endregion

        #region Constructors
        public ExpandableLabel()
        {
            isExpanded = false;
        }
        #endregion

        #region Private Methods
        private static BindableProperty CreateBindableProperty<T>(string propertyName)
        {
            return CreateBindableProperty<T>(propertyName, default(T));
        }

        private static BindableProperty CreateBindableProperty<T>(string propertyName, T defaultValue)
        {
            return BindableProperty.Create(propertyName: propertyName,
                                           returnType: typeof(T),
                                           declaringType: typeof(ExpandableLabel),
                                           defaultValue: defaultValue);
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            switch (propertyName)
            {
                case nameof(ShortText):
                    Text = ShortText;
                    break;
                case nameof(IsExpandable):
                    if (IsExpandable)
                        Clicked += OnClicked;
                    else
                        Clicked -= OnClicked;
                    break;
            }

            base.OnPropertyChanged(propertyName);
        }

        private async void OnClicked(object sender, EventArgs e)
        {
            if (isExpanded)
                return;

            await ExpandLabel();
            await ContractLabel();
        }

        private async Task ExpandLabel()
        {
            isExpanded = true;
            await AnimateTextChange(ExpandedText);
            await Task.Delay(EXPANDED_DURATION);
        }

        private async Task ContractLabel()
        {
            await AnimateTextChange(ShortText);
            isExpanded = false;
        }

        private async Task AnimateTextChange(string newText)
        {
            Text = String.Empty;
            await Task.Delay(ANIMATION_DURATION);
            Text = newText;
        }
        #endregion
    }
}
