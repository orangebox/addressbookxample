﻿using System;
using System.Collections;
using System.Collections.Specialized;
using Xamarin.Forms;

/**
 * NOTE: This code was taken from:
 * https://forums.xamarin.com/discussion/21635/xforms-needs-an-itemscontrol/p2
 */
namespace Xample.Bcl.Xamarin.Controls
{
    public delegate void RepeaterViewItemAddedEventHandler(object sender, RepeaterViewItemAddedEventArgs args);

    // in lieu of an actual Xamarin Forms ItemsControl, this is a heavily modified version of code from https://forums.xamarin.com/discussion/21635/xforms-needs-an-itemscontrol
    public class RepeaterView : StackLayout
    {
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
          propertyName: nameof(ItemsSource),
          returnType: typeof(IEnumerable),
          declaringType: typeof(RepeaterView),
          defaultValue: null,
          defaultBindingMode: BindingMode.OneWay,
          propertyChanged: ItemsChanged);

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create(
          propertyName: nameof(ItemTemplate),
          returnType: typeof(DataTemplate),
          declaringType: typeof(RepeaterView),
          defaultValue: default(DataTemplate));

        public event RepeaterViewItemAddedEventHandler ItemCreated;

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        private static void ItemsChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var oldValueAsEnumerable = oldValue as IEnumerable;
            var newValueAsEnumerable = newValue as IEnumerable;
            var control = (RepeaterView)bindable;

            if (oldValue is INotifyCollectionChanged oldObservableCollection)
                oldObservableCollection.CollectionChanged -= control.OnItemsSourceCollectionChanged;

            if (newValue is INotifyCollectionChanged newObservableCollection)
                newObservableCollection.CollectionChanged += control.OnItemsSourceCollectionChanged;

            control.Children.Clear();

            if (newValueAsEnumerable != null)
            {
                foreach (var item in newValueAsEnumerable)
                {
                    var view = control.CreateChildViewFor(item);
                    control.Children.Add(view);
                    control.OnItemCreated(view);
                }
            }

            control.UpdateChildrenLayout();
            control.InvalidateLayout();
        }

        protected virtual void OnItemCreated(View view)
        {
            ItemCreated?.Invoke(this, new RepeaterViewItemAddedEventArgs(view, view.BindingContext));
        }

        private void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var invalidate = false;

            if (e.OldItems != null)
            {
                Children.RemoveAt(e.OldStartingIndex);
                invalidate = true;
            }

            if (e.NewItems != null)
            {
                for (var i = 0; i < e.NewItems.Count; ++i)
                {
                    var item = e.NewItems[i];
                    var view = CreateChildViewFor(item);

                    Children.Insert(i + e.NewStartingIndex, view);
                    OnItemCreated(view);
                }

                invalidate = true;
            }

            if (invalidate)
            {
                UpdateChildrenLayout();
                InvalidateLayout();
            }
        }

        private View CreateChildViewFor(object item)
        {
            if (ItemTemplate == null)
                throw new NullReferenceException("The ItemTemplate is null");
            ItemTemplate.SetValue(BindingContextProperty, item);
            return (View)ItemTemplate.CreateContent();
        }
    }

    public class RepeaterViewItemAddedEventArgs : EventArgs
    {
        private readonly View view;
        public View View
        {
            get { return view; }
        }

        private readonly object model;
        public object Model
        {
            get { return model; }
        }

        public RepeaterViewItemAddedEventArgs(View view, object model)
        {
            this.view = view;
            this.model = model;
        }

    }
}
