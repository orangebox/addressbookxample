﻿using System;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.Controls
{
    public enum AutoCapitalizationType
    {
        Sentences,
        Words,
        All,
        None,
    }

    public class FreeTextEntry : Entry
    {
        #region Bindable Properties
        public static readonly BindableProperty EnableSpellCheckProperty = CreateBindableProperty<Boolean>(nameof(EnableSpellCheck));
        public static readonly BindableProperty EnableAutoCorrectProperty = CreateBindableProperty<Boolean>(nameof(EnableAutoCorrect));
        public static readonly BindableProperty CapitalizationTypeProperty = CreateBindableProperty<AutoCapitalizationType>(nameof(CapitalizatonType));
        #endregion

        #region Variables
        private const bool DEFAULT_ENABLE_SPELL_CHECK_VALUE = true;
        private const bool DEFAULT_ENABLE_AUTO_CORRECT_VALUE = true;
        private const AutoCapitalizationType DEFAULT_AUTO_CAPITALIZATION_VALUE = AutoCapitalizationType.Sentences;

        public bool EnableSpellCheck
        {
            get { return (Boolean)GetValue(EnableSpellCheckProperty); }
            set { SetValue(EnableSpellCheckProperty, value); }
        }
        public bool EnableAutoCorrect
        {
            get { return (Boolean)GetValue(EnableAutoCorrectProperty); }
            set { SetValue(EnableAutoCorrectProperty, value); }
        }
        public AutoCapitalizationType CapitalizatonType
        {
            get { return (AutoCapitalizationType)GetValue(CapitalizationTypeProperty); }
            set { SetValue(CapitalizationTypeProperty, value); }
        }
        #endregion

        #region Constructors
        public FreeTextEntry()
        {
            EnableSpellCheck = DEFAULT_ENABLE_SPELL_CHECK_VALUE;
            EnableAutoCorrect = DEFAULT_ENABLE_AUTO_CORRECT_VALUE;
            CapitalizatonType = DEFAULT_AUTO_CAPITALIZATION_VALUE;
        }
        #endregion

        #region Private Methods
        private static BindableProperty CreateBindableProperty<T>(string propertyName)
        {
            return CreateBindableProperty<T>(propertyName, default(T));
        }

        private static BindableProperty CreateBindableProperty<T>(string propertyName, T defaultValue)
        {
            return BindableProperty.Create(propertyName: propertyName,
                                           returnType: typeof(T),
                                           declaringType: typeof(FreeTextEntry),
                                           defaultValue: defaultValue);
        }
        #endregion

        #region Public Static Methods
        public static bool IsTypingPropertyName(string propertyName)
        {
            return propertyName == EnableSpellCheckProperty.PropertyName ||
                   propertyName == EnableAutoCorrectProperty.PropertyName ||
                   propertyName == CapitalizationTypeProperty.PropertyName;
        }
        #endregion

    }
}
