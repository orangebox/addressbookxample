﻿using System;

namespace Xample.Bcl.Xamarin.Iconography
{
    public class IconAttribute : Attribute
    {
        public string IconPath { get; }

        public IconAttribute(string iconPath)
        {
            IconPath = iconPath;
        }

    }
}
