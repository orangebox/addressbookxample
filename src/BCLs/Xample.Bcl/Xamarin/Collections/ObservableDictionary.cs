﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Xample.Bcl.Xamarin.Collections
{
    public class ObservableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        #region Constructors
        public ObservableDictionary() { }
        public ObservableDictionary(IDictionary<TKey, TValue> dictionary)
        {
            foreach (var pair in dictionary)
                base.Add(pair.Key, pair.Value);
        }
        #endregion

        #region Private methods
        private KeyValuePair<TKey, TValue> BuildKvp(TKey key)
        {
            return BuildKvp(key, this[key]);
        }

        private KeyValuePair<TKey, TValue> BuildKvp(TKey key, TValue value)
        {
            return new KeyValuePair<TKey, TValue>(key, value);
        }
        #endregion

        #region Public methods
        public new virtual void Add(TKey key, TValue value)
        {
            base.Add(key, value);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, BuildKvp(key, value)));
            this.OnPropertyChanged(new PropertyChangedEventArgs(nameof(base.Count)));
        }

        public new virtual void Clear()
        {
            base.Clear();
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            this.OnPropertyChanged(new PropertyChangedEventArgs(nameof(base.Count)));
        }

        public new virtual bool Remove(TKey key)
        {
            var removedItem = BuildKvp(key);
            var isRemoved = base.Remove(key);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem));
            this.OnPropertyChanged(new PropertyChangedEventArgs(nameof(base.Count)));
            return isRemoved;
        }
        #endregion

        #region INotifyCollectionChanged implementation
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e) => this.CollectionChanged?.Invoke(this, e);
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e) => this.RaiseCollectionChanged(e);
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { this.PropertyChanged += value; }
            remove { this.PropertyChanged -= value; }
        }
        private void RaisePropertyChanged(PropertyChangedEventArgs e) => this.PropertyChanged?.Invoke(this, e);
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e) => this.RaisePropertyChanged(e);
        #endregion
    }
}
