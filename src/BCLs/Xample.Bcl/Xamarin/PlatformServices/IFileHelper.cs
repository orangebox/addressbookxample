﻿namespace Xample.Bcl.Xamarin.PlatformServices
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
