﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xample.Bcl.Xamarin.PlatformServices
{
    public class Email
    {
        #region Variables
        private readonly List<String> toList;
        private readonly List<String> ccList;
        private readonly List<String> bccList;

        public IReadOnlyCollection<String> Recipients { get { return toList; } }
        public IReadOnlyCollection<String> CcRecipients { get { return ccList; } }
        public IReadOnlyCollection<String> BccRecipients { get { return bccList; } }
        public string Subject { get; set; }
        public string Body { get; set; }
        #endregion

        #region Constructors
        public Email()
        {
            toList = new List<string>();
            ccList = new List<string>();
            bccList = new List<string>();
        }
        #endregion

        #region Public Methods
        public void AddRecipient(string email)
        {
            toList.Add(email);
        }

        public void AddRecipients(IEnumerable<String> emails)
        {
            AddRecipients(emails.ToArray());
        }

        public void AddRecipients(params string[] emails)
        {
            toList.AddRange(emails);
        }

        public void AddCcRecipient(string email)
        {
            ccList.Add(email);
        }

        public void AddCcRecipients(params string[] emails)
        {
            ccList.AddRange(emails);
        }

        public void AddBccRecipient(string email)
        {
            bccList.Add(email);
        }

        public void AddBccRecipients(params string[] emails)
        {
            bccList.AddRange(emails);
        }
        #endregion

    }
}
