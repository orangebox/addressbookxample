﻿namespace Xample.Bcl.Xamarin.PlatformServices
{
    public interface IProcessKiller
    {
        void Kill();
    }
}
