﻿using System;

namespace Xample.Bcl.Xamarin.PlatformServices
{
    public enum XampleLogLevel
    {
        Trace,
        Debug,
        Info,
        Warning,
        Error
    }

    public interface ILogger
    {
        void Initialize(Type callingClass);

        void Log(XampleLogLevel level, string message);

        void Trace(string message);

        void Debug(string message);

        void Info(string message);

        void Warning(string message);

        void Error(string message);
        void Error(Exception exception);
        void Error(string message, Exception exception);
    }

    public class DesignLogger : ILogger
    {
        public void Initialize(Type callingClass) { }
        public void Log(XampleLogLevel level, string message) { }
        public void Trace(string message) { }
        public void Debug(string message) { }
        public void Info(string message) { }
        public void Warning(string message) { }
        public void Error(string message) { }
        public void Error(Exception exception) { }
        public void Error(string message, Exception exception) { }
    }
}
