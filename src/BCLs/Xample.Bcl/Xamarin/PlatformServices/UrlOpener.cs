﻿using System;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.PlatformServices
{
    public interface IUrlOpener
    {
        void OpenUrl(string urlPath);
        void OpenUrl(Uri url);
    }

    public class UrlOpener : IUrlOpener
    {
        public void OpenUrl(string urlPath)
        {
            OpenUrl(new Uri(urlPath));
        }

        public void OpenUrl(Uri url)
        {
            Device.OpenUri(url);
        }
    }

}
