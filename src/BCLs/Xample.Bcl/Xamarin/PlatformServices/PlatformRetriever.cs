﻿using System;
using Xamarin.Forms;

namespace Xample.Bcl.Xamarin.PlatformServices
{
    public enum Platform
    {
        Android,
        iOS,
        None
    }

    public interface IPlatformRetriever
    {
        Platform GetPlatform();
    }

    public class DesignPlatformRetriever : IPlatformRetriever
    {
        public Platform GetPlatform()
        {
            return Platform.None;
        }
    }

    public class PlatformRetriever : IPlatformRetriever
    {
        public Platform GetPlatform()
        {
            var platform = Device.RuntimePlatform;
            switch (platform)
            {
                case Device.Android:
                    return Platform.Android;
                case Device.iOS:
                    return Platform.iOS;
                default:
                    throw new NotSupportedException($"Unhandled platform: {platform}");
            }
        }
    }
}
