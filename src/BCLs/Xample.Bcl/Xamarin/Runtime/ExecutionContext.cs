﻿using System;

namespace Xample.Bcl.Xamarin.Runtime
{
    public static class ExecutionContext
    {
        private static bool inDesignMode = true;
        public static bool InDesignMode
        {
            get { return inDesignMode; }
            set
            {
                if (value == true)
                    throw new ArgumentException("You cannot manually set this to true", nameof(inDesignMode));
                inDesignMode = value;
            }
        }

    }
}
