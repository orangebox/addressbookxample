﻿using System;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Xample.Bcl.Text
{
    public class NumericStringFormatter
    {
        #region Variables
        private readonly CultureInfo culture;
        #endregion

        #region Constructors
        public NumericStringFormatter(string cultureString)
            : this(new CultureInfo(cultureString))
        {

        }

        public NumericStringFormatter(CultureInfo ci)
        {
            culture = ci;
        }
        #endregion

        #region Private Methods
        private string BuildIsNumberRegularExpression()
        {
            var decimalSeparator = culture.NumberFormat.NumberDecimalSeparator;

            //Example: ^\d+\.?\d*$
            var pattern = $@"^\d+\{decimalSeparator}?\d*$";

            return pattern;
        }

        private string BuildIsFormattedNumberRegularExpression()
        {
            var negativeSign = culture.NumberFormat.NegativeSign;
            var thousandsSeparator = culture.NumberFormat.CurrencyGroupSeparator;
            var decimalSeparator = culture.NumberFormat.CurrencyDecimalSeparator;

            //Example: -?\d{1,3}(\,\d{3})*(\.\d+)?
            var pattern = $@"{negativeSign}?\d{{1,3}}(\{thousandsSeparator}\d{{3}})*(\{decimalSeparator}\d+)?";

            return pattern;
        }

        private Match MatchAgainstIsNumberRegex(string text)
        {
            var pattern = BuildIsNumberRegularExpression();
            var match = Regex.Match(text, pattern);
            return match;
        }

        private Match MatchAgainstIsFormattedNumberRegex(string text)
        {
            var pattern = BuildIsFormattedNumberRegularExpression();
            var match = Regex.Match(text, pattern);
            return match;
        }
        #endregion

        #region Public Methods
        public string ApplyFormatting(string originalText, string format)
        {
            var matchResult = MatchAgainstIsNumberRegex(originalText);
            if (!matchResult.Success)
                return originalText;

            var valueDouble = Convert.ToDouble(originalText, culture);
            var formattedDouble = String.Format(culture, format, valueDouble);

            return formattedDouble;
        }

        public string RemoveFormatting(string originalText)
        {
            if (Double.TryParse(originalText, out double r))
                return originalText;

            var matchResult = MatchAgainstIsFormattedNumberRegex(originalText);
            if (!matchResult.Success)
                return originalText;

            var matchResultDouble = Convert.ToDouble(matchResult.Value, culture);
            return matchResultDouble.ToString(culture);
        }
        #endregion
    }
}
