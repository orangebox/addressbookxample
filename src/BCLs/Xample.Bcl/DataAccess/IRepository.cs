﻿using System;
using System.Collections.Generic;

namespace Xample.Bcl.DataAccess
{
    public interface IRepository<T> where T : IEntity
    {
		// create
		int Add(T entity);
		int AddMany(IEnumerable<T> entities);

		// read
		IEnumerable<T> GetAll();
		T Get(int id);
        T GetLast();
		IEnumerable<T> Find(Func<T, bool> predicate);
        T FindFirst(Func<T, bool> predicate);
        T FindFirstOrDefault(Func<T, bool> predicate);
        T FindSingle(Func<T, bool> predicate);
        T FindSingleOrDefault(Func<T, bool> predicate);

		// update
		int Update(T entity);
		int UpdateMany(IEnumerable<T> entities);

        // delete
        int Delete(T entity);
        int Delete(Func<T, bool> predicate);
        int DeleteMany(IEnumerable<T> entities);
        int DeleteAll();
    }
}
