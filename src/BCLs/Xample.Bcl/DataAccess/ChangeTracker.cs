﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xample.Bcl.Extensions;

namespace Xample.Bcl.DataAccess
{
    public interface IChangeTracker<T>
    {
        IReadOnlyCollection<T> OriginalItems { get; }
        IReadOnlyCollection<T> AddedItems { get; }
        IReadOnlyCollection<T> UpdatedItems { get; }
        IReadOnlyCollection<T> DeletedItems { get; }
        bool HasChanges { get; }

        void Initialize(IEnumerable<T> originals);
        void AddNewItem(T newItem);
        void UpdateExistingItem(T updatedItem);
        void DeleteExistingItem(T deletedItem);

        void Reset();
        void SyncChanges();
    }

    public class ChangeTracker<T> : IChangeTracker<T>
    {
        #region Variables
        private IDictionary<Int32, T> originalItemsDictionary;
        private readonly IDictionary<Int32, T> addedItemsDictionary;
        private readonly IDictionary<Int32, T> updatedItemsDictionary;
        private readonly IDictionary<Int32, T> deletedItemsDictionary;

        public IReadOnlyCollection<T> OriginalItems { get { return originalItemsDictionary.Values.ToList(); } }
        public IReadOnlyCollection<T> AddedItems { get { return addedItemsDictionary.Values.ToList(); } }
        public IReadOnlyCollection<T> UpdatedItems { get { return updatedItemsDictionary.Values.ToList(); } }
        public IReadOnlyCollection<T> DeletedItems { get { return deletedItemsDictionary.Values.ToList(); } }
        public bool HasChanges
        {
            get
            {
                return addedItemsDictionary.Any() ||
                       updatedItemsDictionary.Any() ||
                       deletedItemsDictionary.Any();
            }
        }
        #endregion

        #region Constructors
        public ChangeTracker()
        {
            addedItemsDictionary = new Dictionary<Int32, T>();
            updatedItemsDictionary = new Dictionary<Int32, T>();
            deletedItemsDictionary = new Dictionary<Int32, T>();
        }

        public ChangeTracker(IEnumerable<T> originals)
            : this()
        {
            Initialize(originals);
        }
        #endregion

        #region Private Methods
        private static void RemoveIfExists(IDictionary<Int32, T> dictionary, int key)
        {
            if (dictionary.ContainsKey(key))
                dictionary.Remove(key);
        }
        #endregion

        #region IChangeTracker<T> Methods
        public void Initialize(IEnumerable<T> originals)
        {
            var originalsDictionary = originals.ToDictionary(o => o.GetHashCode(), o => o);
            originalItemsDictionary = new Dictionary<Int32, T>(originalsDictionary);
        }

        public void AddNewItem(T newItem)
        {
            var key = newItem.GetHashCode();

            var isOriginalItem = originalItemsDictionary.ContainsKey(key);
            var isAddedItem = addedItemsDictionary.ContainsKey(key);
            if (!isOriginalItem && !isAddedItem)
                addedItemsDictionary.Add(key, newItem);

            RemoveIfExists(deletedItemsDictionary, key);
        }

        public void UpdateExistingItem(T updatedItem)
        {
            var key = updatedItem.GetHashCode();

            var isOriginalItem = originalItemsDictionary.ContainsKey(key);
            var isUpdatedItem = updatedItemsDictionary.ContainsKey(key);
            if (isOriginalItem && !isUpdatedItem)
                updatedItemsDictionary.Add(key, updatedItem);

            RemoveIfExists(deletedItemsDictionary, key);
        }

        public void DeleteExistingItem(T deletedItem)
        {
            var key = deletedItem.GetHashCode();

            var isOriginalItem = originalItemsDictionary.ContainsKey(key);
            var isDeletedItem = deletedItemsDictionary.ContainsKey(key);
            if (isOriginalItem && !isDeletedItem)
                deletedItemsDictionary.Add(key, deletedItem);

            RemoveIfExists(addedItemsDictionary, key);
            RemoveIfExists(updatedItemsDictionary, key);
        }

        public void Reset()
        {
            addedItemsDictionary.Clear();
            updatedItemsDictionary.Clear();
            deletedItemsDictionary.Clear();
        }

        public void SyncChanges()
        {
            originalItemsDictionary.AddRange(addedItemsDictionary);
            originalItemsDictionary.UpdateRange(updatedItemsDictionary);
            originalItemsDictionary.RemoveRange(deletedItemsDictionary);

            Reset();
        }
        #endregion
    }
}
