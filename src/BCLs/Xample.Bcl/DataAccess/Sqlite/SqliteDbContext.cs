﻿using SQLite;

namespace Xample.Bcl.DataAccess.Sqlite
{
    public abstract class SqliteDbContext : DbContextBase
    {
        protected readonly string dbPath;
        public SQLiteConnection Connection { get { return new SQLiteConnection(dbPath); } }

        protected SqliteDbContext(string path)
        {
            dbPath = path;
        }

    }
}
