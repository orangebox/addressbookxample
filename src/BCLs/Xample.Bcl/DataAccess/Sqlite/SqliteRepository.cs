﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xample.Bcl.DataAccess.Sqlite
{
    public class SqliteRepository<T> : IQueryableRepository<T>, IRepository<T> 
                                       where T : class, IEntity, new()
    {
        private readonly SqliteDbContext dbContext;

        public SqliteRepository(SqliteDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        #region Create
        public int Add(T entity)
        {
            var numRows = 0;
            using (var cnxn = dbContext.Connection)
            {
                numRows = cnxn.Insert(entity);
            }
            return numRows;
        }

        public int AddMany(IEnumerable<T> entities)
        {
            var numRows = 0;
            using (var cnxn = dbContext.Connection)
            {
                numRows = cnxn.InsertAll(entities);
            }
            return numRows;
        }
		#endregion

		#region Read
		public IEnumerable<T> GetAll()
		{
			var collection = new List<T>();
			using (var cnxn = dbContext.Connection)
			{
				var result = cnxn.Table<T>();
				collection.AddRange(result);
			}
			return collection;
		}

        public T GetLast()
        {
            T entity;
            using (var cnxn = dbContext.Connection)
            {
                entity = cnxn.Table<T>().OrderByDescending(e => e.Id).FirstOrDefault();
            }
            return entity;
        }

        public T Get(int id)
		{
			T entity;
			using (var cnxn = dbContext.Connection)
			{
				entity = cnxn.Get<T>(id);
			}
			return entity;
		}

		public IEnumerable<T> Find(Func<T, bool> predicate)
		{
			var collection = new List<T>();
			using (var cnxn = dbContext.Connection)
			{
				var results = cnxn.Table<T>().Where(predicate);
				collection.AddRange((IEnumerable<T>)results);
			}
			return collection;
		}

		public T FindFirst(Func<T, bool> predicate)
		{
            T entity;
            using(var cnxn = dbContext.Connection)
            {
                entity = cnxn.Table<T>().First(predicate);
            }
            return entity;
		}

		public T FindFirstOrDefault(Func<T, bool> predicate)
		{
			T entity;
			using (var cnxn = dbContext.Connection)
			{
				entity = cnxn.Table<T>().FirstOrDefault(predicate);
			}
			return entity;
		}

		public T FindSingle(Func<T, bool> predicate)
		{
			T entity;
			using (var cnxn = dbContext.Connection)
			{
				entity = cnxn.Table<T>().Single(predicate);
			}
			return entity;
		}

		public T FindSingleOrDefault(Func<T, bool> predicate)
		{
			T entity;
			using (var cnxn = dbContext.Connection)
			{
				entity = cnxn.Table<T>().SingleOrDefault(predicate);
			}
			return entity;
		}
		#endregion

		#region Update
		public int Update(T entity)
		{
			var numRows = 0;
			using (var cnxn = dbContext.Connection)
			{
				numRows = cnxn.Update(entity);
			}
			return numRows;
		}

		public int UpdateMany(IEnumerable<T> entities)
		{
			var numRows = 0;
			using (var cnxn = dbContext.Connection)
			{
				numRows = cnxn.UpdateAll(entities);
			}
			return numRows;
		}
		#endregion

		#region Delete
		public int Delete(T entity)
        {
            var numRows = 0;
            using (var cnxn = dbContext.Connection)
            {
                numRows = cnxn.Delete(entity);
            }
            return numRows;
        }

		public int Delete(Func<T, bool> predicate)
		{
            var entities = Find(predicate);
            return DeleteMany(entities);
		}

        public int DeleteMany(IEnumerable<T> entities)
        {
            var numRows = 0;
            using (var cnxn = dbContext.Connection)
            {
                foreach (var entity in entities)
                {
                    numRows += cnxn.Delete(entity);
                }
            }
            return numRows;
        }

        public int DeleteAll()
        {
            var numRows = 0;
            using (var cnxn = dbContext.Connection)
            {
                numRows = cnxn.DeleteAll<T>();
            }
            return numRows;
        }
        #endregion

        #region Custom SQL
        public IEnumerable<T> ExecuteCommand(string sql)
        {
            var results = new List<T>();
            using(var cnxn = dbContext.Connection)
            {
                var cmd = cnxn.CreateCommand(sql);
                results = cmd.ExecuteQuery<T>();
            }
            return results;
        }
        #endregion

    }
}
