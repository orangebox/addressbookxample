﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Xample.Bcl.DataAccess.Rest
{
    public interface IRestContext
    {
        void SetDefaultRequestHeaders(string name, string value);
        void SetDefaultRequestHeaders(string name, IEnumerable<String> values);

        Task<String> GetStringAsync(string path);
        Task<HttpResponseMessage> GetAsync(string path);

        Task<HttpResponseMessage> PostAsync(string path, string contentJson);

        Task<HttpResponseMessage> PutAsync(string path, string contentJson);

        Task<HttpResponseMessage> DeleteAsync(string path);
    }

    public class RestContext : IRestContext, IDisposable
    {
        private readonly HttpClient httpClient;

        public RestContext()
        {
            httpClient = new HttpClient();
        }

        // Public Methods
        public void SetDefaultRequestHeaders(string name, string value)
        {
            httpClient.DefaultRequestHeaders.Add(name, value);
        }

        public void SetDefaultRequestHeaders(string name, IEnumerable<String> values)
        {
            httpClient.DefaultRequestHeaders.Add(name, values);
        }

        // GET
        public Task<String> GetStringAsync(string path)
        {
            var json = httpClient.GetStringAsync(path);
            return json;
        }

        public Task<HttpResponseMessage> GetAsync(string path)
        {
            var responseMessage = httpClient.GetAsync(path);
            return responseMessage;
        }

        // POST
        public Task<HttpResponseMessage> PostAsync(string path, string contentJson)
        {
            var content = new StringContent(contentJson);
            var responseMessage = httpClient.PostAsync(path, content);

            return responseMessage;
        }

        // PUT
        public Task<HttpResponseMessage> PutAsync(string path, string contentJson)
        {
            var content = new StringContent(contentJson);
            var responseMessage = httpClient.PutAsync(path, content);

            return responseMessage;
        }

        // DELETE
        public Task<HttpResponseMessage> DeleteAsync(string path)
        {
            var responseMessage = httpClient.DeleteAsync(path);
            return responseMessage;
        }

        // IDisposable Implementation
        ~RestContext()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            httpClient.Dispose();
        }
    }
}
