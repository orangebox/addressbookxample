﻿using System.Collections.Generic;

namespace Xample.Bcl.DataAccess
{
    public interface IQueryableRepository<T> where T : IEntity
    {
		IEnumerable<T> ExecuteCommand(string sql);
    }
}
