﻿namespace Xample.Bcl.DataAccess
{
    public abstract class DbContextBase
    {
        public abstract void ClearDatabase();
        protected abstract bool DatabaseExists();
        protected abstract void CreateDatabase();
        protected abstract void PopulateDatabase();
        protected abstract void UpgradeDatabase();

        public void InitDb()
        {
            var dbExists = DatabaseExists();

#if DEBUG
            ClearDatabase();
#endif
            CreateDatabase();

			if (!dbExists)
				PopulateDatabase();

            UpgradeDatabase();
        }

    }
}
