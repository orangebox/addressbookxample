﻿namespace Xample.Bcl.DataAccess.Versioning
{
    public interface IDatabaseUpgradeWorkflowExecutor
    {
        void ProcessUpgrade();
    }

    public class DatabaseUpgradeWorkflowExecutor : IDatabaseUpgradeWorkflowExecutor
    {
        private readonly IDatabaseVersionDataService databaseVersionDataService;
        private readonly IUpgradeProcessor upgradeProcessor;

        public DatabaseUpgradeWorkflowExecutor(IDatabaseVersionDataService dvds, IUpgradeProcessor ue)
        {
            databaseVersionDataService = dvds;
            upgradeProcessor = ue;
        }

        public void ProcessUpgrade()
        {
            var currentVersion = databaseVersionDataService.GetCurrentVersion();

            var nextVersion = upgradeProcessor.HandleUpgrade(currentVersion);

            if (nextVersion > currentVersion)
                databaseVersionDataService.SaveVersion(nextVersion);
        }

    }
}
