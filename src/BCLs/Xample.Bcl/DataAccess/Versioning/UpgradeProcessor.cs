﻿namespace Xample.Bcl.DataAccess.Versioning
{
    public interface IUpgradeProcessor
    {
        DatabaseVersion HandleUpgrade(DatabaseVersion currentVersion);
    }

    public class GenericInMemoryUpgradeProcessor : IUpgradeProcessor
    {
        public DatabaseVersion HandleUpgrade(DatabaseVersion currentVersion)
        {
            return currentVersion;
        }
    }

}
