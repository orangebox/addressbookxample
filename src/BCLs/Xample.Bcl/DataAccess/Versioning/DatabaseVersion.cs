﻿using SQLite;

namespace Xample.Bcl.DataAccess.Versioning
{
    public class DatabaseVersion : IEntity
    {
        #region Variables
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [NotNull, Unique]
        public int Version { get; set; }
        #endregion

        #region Constructors
        public DatabaseVersion()
        {

        }

        public DatabaseVersion(int version)
        {
            Version = version;
        }
        #endregion

        #region Operator Overrides
        public static DatabaseVersion operator ++(DatabaseVersion version)
        {
            var nextVersion = version.Version++;
            return new DatabaseVersion(nextVersion);
        }

        public static bool operator >(DatabaseVersion leftVersion, DatabaseVersion rightVersion)
        {
            return leftVersion.Version > rightVersion.Version;
        }

        public static bool operator <(DatabaseVersion leftVersion, DatabaseVersion rightVersion)
        {
            return leftVersion.Version < rightVersion.Version;
        }

        public static bool operator >=(DatabaseVersion leftVersion, DatabaseVersion rightVersion)
        {
            return leftVersion.Version >= rightVersion.Version;
        }

        public static bool operator <=(DatabaseVersion leftVersion, DatabaseVersion rightVersion)
        {
            return leftVersion.Version <= rightVersion.Version;
        }

        public static bool operator ==(DatabaseVersion leftVersion, DatabaseVersion rightVersion)
        {
            return leftVersion.Version == rightVersion.Version;
        }

        public static bool operator !=(DatabaseVersion leftVersion, DatabaseVersion rightVersion)
        {
            return leftVersion.Version != rightVersion.Version;
        }
        #endregion

        #region Equality Overrides
        public override int GetHashCode()
        {
            return Version.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var that = obj as DatabaseVersion;
            if (that == null)
                return false;

            return this.Version == that.Version;
        }
        #endregion
    }
}
