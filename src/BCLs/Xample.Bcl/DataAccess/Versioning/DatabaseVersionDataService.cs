﻿using System;

namespace Xample.Bcl.DataAccess.Versioning
{
    public interface IDatabaseVersionDataService
    {
        DatabaseVersion SaveVersion(DatabaseVersion version);
        DatabaseVersion GetCurrentVersion();
    }

    public class DatabaseVersionDataService : IDatabaseVersionDataService
    {
        private readonly IRepository<DatabaseVersion> databaseVersionRepository;

        public DatabaseVersionDataService(IRepository<DatabaseVersion> dvr)
        {
            databaseVersionRepository = dvr;
        }

        public DatabaseVersion SaveVersion(DatabaseVersion version)
        {
            if (version.Id > 0)
                throw new ArgumentException($"{nameof(version)} has an ID of {version.Id}", nameof(version));

            databaseVersionRepository.Add(version);
            return version;
        }

        public DatabaseVersion GetCurrentVersion()
        {
            var version = databaseVersionRepository.GetLast();
            return version;
        }

    }
}
