﻿namespace Xample.Bcl.DataAccess
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
