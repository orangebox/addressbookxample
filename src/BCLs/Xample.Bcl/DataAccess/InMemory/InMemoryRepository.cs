﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xample.Bcl.DataAccess.InMemory
{
    public class InMemoryRepository<T> : IRepository<T> where T : IEntity
    {
        private readonly InMemoryDbContext dbContext;

        public InMemoryRepository(InMemoryDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        #region Create
        public int Add(T entity)
        {
            return dbContext.Insert(entity);
        }

        public int AddMany(IEnumerable<T> entities)
        {
            return dbContext.Insert(entities);
        }
		#endregion

		#region Read
		public T Get(int id)
		{
			return dbContext.Get<T>(id);
		}

		public IEnumerable<T> GetAll()
		{
			return dbContext.Table<T>();
		}

        public T GetLast()
        {
            return dbContext.Table<T>().OrderByDescending(e => e.Id).FirstOrDefault();
        }

		public IEnumerable<T> Find(Func<T, bool> predicate)
		{
			return dbContext.Table<T>().Where(predicate);
		}

		public T FindFirst(Func<T, bool> predicate)
        {
            return dbContext.Table<T>().First(predicate);
        }

		public T FindFirstOrDefault(Func<T, bool> predicate)
        {
            return dbContext.Table<T>().FirstOrDefault(predicate);
        }

		public T FindSingle(Func<T, bool> predicate)
        {
            return dbContext.Table<T>().Single(predicate);
        }

		public T FindSingleOrDefault(Func<T, bool> predicate)
        {
            return dbContext.Table<T>().SingleOrDefault(predicate);
        }
		#endregion

		#region Update
		public int Update(T entity)
		{
			return dbContext.Update(entity);
		}

		public int UpdateMany(IEnumerable<T> entities)
		{
			return dbContext.Update(entities);
		}
		#endregion
		
        #region Delete
		public int Delete(T entity)
        {
            return dbContext.Delete(entity);
        }

        public int Delete(Func<T, bool> predicate)
        {
            var entities = Find(predicate).ToList();
			return DeleteMany(entities);
        }

        public int DeleteMany(IEnumerable<T> entities)
        {
            return dbContext.DeleteMany(entities);
        }

        public int DeleteAll()
        {
            return dbContext.DeleteAll<T>();
        }
        #endregion

    }
}
