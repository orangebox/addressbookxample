﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xample.Bcl.DataAccess.InMemory
{
    public abstract class InMemoryDbContext : DbContextBase
    {
        /// <summary>
        /// Keeps track of each table's last inserted ID.
        /// The KEY corresponds to the table.
        /// The VALUE corresponds to the last inserted ID.
        /// </summary>
        private readonly Dictionary<Type, Int32> lastInsertedIds;
        private readonly List<IEntity> tables;

        protected InMemoryDbContext()
        {
            lastInsertedIds = new Dictionary<Type, Int32>();
            tables = new List<IEntity>();
        }

        #region Private methods
        private int IncrementId<T>() where T : IEntity
        {
            if (!lastInsertedIds.ContainsKey(typeof(T)))
                lastInsertedIds[typeof(T)] = 0;
            lastInsertedIds[typeof(T)] += 1;
            return lastInsertedIds[typeof(T)];
        }
        #endregion

        #region DbContextBase Implementation
        public override void ClearDatabase()
        {
            tables.Clear();
            lastInsertedIds.Clear();
        }

        protected override bool DatabaseExists()
        {
            return tables.Any();
        }

        protected override void CreateDatabase()
        {

        }

        protected override void UpgradeDatabase()
        {
            
        }
        #endregion

        #region Create
        public int Insert<T>(T entity) where T : IEntity
        {
            entity.Id = IncrementId<T>();
            tables.Add(entity);
            return 1;
        }

        public int Insert<T>(IEnumerable<T> entities) where T : IEntity
        {
            foreach (var entity in entities)
            {
                entity.Id = IncrementId<T>();
            }
            tables.AddRange((IEnumerable<IEntity>)entities);
            return entities.Count();
        }
        #endregion

        #region Read
        public T Get<T>(int id) where T : IEntity
        {
            return Table<T>().FirstOrDefault(ent => ent.Id == id);
        }

        public IQueryable<T> Table<T>() where T : IEntity
        {
            return tables.OfType<T>().AsQueryable();
        }
        #endregion

        #region Update
        public int Update<T>(T entity) where T : IEntity
        {
            var entityToUpdate = Get<T>(entity.Id);
            //ignore the warning here - it'll never be a comparison to a value type 
            //because we're restricting T to be an IEntity
#pragma warning disable RECS0017
            if (entityToUpdate == null)
                return 0;
#pragma warning restore RECS0017

            var entityIndex = tables.IndexOf(entityToUpdate);
            tables[entityIndex] = entity;

            return 1;
        }

        public int Update<T>(IEnumerable<T> entities) where T : IEntity
        {
            var count = 0;
            foreach (var entity in entities)
                count += Update(entity);
            return count;
        }
        #endregion

        #region Delete
        public int Delete<T>(T entity) where T : IEntity
        {
            var entityToDelete = Get<T>(entity.Id);
            var isSuccess = tables.Remove(entityToDelete);
            return isSuccess ? 1 : 0;
        }

        public int DeleteMany<T>(IEnumerable<T> entities) where T : IEntity
        {
            var removedCount = 0;
            foreach (var entity in entities.ToList())
            {
                var isSuccess = tables.Remove(entity);
                removedCount += (isSuccess ? 1 : 0);
            }
            return removedCount;
        }

        public int DeleteAll<T>() where T : IEntity
        {
            var entities = tables.OfType<T>().ToList();
            return DeleteMany(entities);
        }
        #endregion

    }
}
