﻿using System.Collections;
using System.Collections.Generic;

namespace Xample.Bcl.Collections
{
    public interface IWritableEntityCollection<TKey, TValue>
    {
        void Add(KeyValuePair<TKey, TValue> keyValuePair);
        void Add(TKey key, TValue item);
    }

    public abstract class ReadOnlyEntityCollection<TKey, TValue>
        : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        #region Variables
        protected readonly Dictionary<TKey, TValue> entities;

        public TValue this[TKey key]
        {
            get { return entities[key]; }
        }

        public ICollection<TKey> Keys
        {
            get { return entities.Keys; }
        }
        #endregion

        #region Constructors
        protected ReadOnlyEntityCollection()
            : this(new Dictionary<TKey, TValue>())
        {

        }

        protected ReadOnlyEntityCollection(IDictionary<TKey, TValue> entities)
        {
            this.entities = new Dictionary<TKey, TValue>(entities);
        }
        #endregion

        #region IEnumerable Implementation
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            foreach(var keyValuePair in entities)
            {
                yield return keyValuePair;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
		#endregion
    }
}
