﻿using System.Threading;
using Xample.Bcl.Xamarin.PlatformServices;

namespace Xample.Bcl.iOS.PlatformServices
{
    public class ProcessKiller : IProcessKiller
    {
        public void Kill()
        {
            Thread.CurrentThread.Abort();
        }

    }
}
