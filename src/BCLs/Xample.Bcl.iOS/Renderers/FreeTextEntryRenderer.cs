﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xample.Bcl.iOS.Renderers;
using Xample.Bcl.Xamarin.Controls;
using Xample.Bcl.Xamarin.Controls.BaseRenderers;

[assembly: ExportRenderer(typeof(FreeTextEntry), typeof(FreeTextEntryRenderer))]
namespace Xample.Bcl.iOS.Renderers
{
    public class FreeTextEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            FreeTextEntryRendererBase.OnElementChanged(Control, e.NewElement, UpdateTypingProperties);
        }

        private void UpdateTypingProperties(FreeTextEntry freeTextEntry)
        {
            Control.SpellCheckingType = GetSpellCheckingTypeFlag(freeTextEntry.EnableSpellCheck);
            Control.AutocorrectionType = GetAutoCorrectFlag(freeTextEntry.EnableAutoCorrect);
            Control.AutocapitalizationType = GetAutoCapitalizationFlag(freeTextEntry.CapitalizatonType);
        }

        private static UITextSpellCheckingType GetSpellCheckingTypeFlag(bool enableSpellCheck)
        {
            if (enableSpellCheck)
                return UITextSpellCheckingType.Yes;
            else
                return UITextSpellCheckingType.No;
        }

        private static UITextAutocorrectionType GetAutoCorrectFlag(bool enableAutoCorrect)
        {
            if (enableAutoCorrect)
                return UITextAutocorrectionType.Yes;
            else
                return UITextAutocorrectionType.No;
        }

        private static UITextAutocapitalizationType GetAutoCapitalizationFlag(AutoCapitalizationType autoCapitalizationType)
        {
            switch (autoCapitalizationType)
            {
                case AutoCapitalizationType.None:
                    return UITextAutocapitalizationType.None;
                case AutoCapitalizationType.Sentences:
                    return UITextAutocapitalizationType.Sentences;
                case AutoCapitalizationType.Words:
                    return UITextAutocapitalizationType.Words;
                case AutoCapitalizationType.All:
                    return UITextAutocapitalizationType.AllCharacters;
                default:
                    throw new NotSupportedException($"Unhandled capitalization type: {autoCapitalizationType}");
            }
        }

    }
}
