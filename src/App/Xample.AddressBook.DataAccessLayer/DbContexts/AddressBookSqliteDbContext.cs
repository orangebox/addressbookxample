﻿using Xample.AddressBook.DataAccessLayer.Entities;
using Xample.AddressBook.DataAccessLayer.Population;
using Xample.Bcl.DataAccess.Sqlite;
using Xample.Bcl.DataAccess.Versioning;
using Xample.Bcl.Ioc;
#if !DEBUG
using System.Linq;
#endif

namespace Xample.AddressBook.DataAccessLayer.DbContexts
{
    public class AddressBookSqliteDbContext : SqliteDbContext
    {
        public AddressBookSqliteDbContext(string dbPath)
            : base(dbPath)
        {

        }

        public override void ClearDatabase()
        {
            using (Connection)
            {
                Connection.DropTable<Contact>();
                Connection.DropTable<DatabaseVersion>();
            }
        }

        protected override bool DatabaseExists()
        {
#if DEBUG
            return false;
#else
			using (Connection)
			{
				var exists = Connection.GetTableInfo(typeof(Contact).Name).Any();
				return exists;
			}
#endif
        }

        protected override void CreateDatabase()
        {
            using (Connection)
            {
                Connection.CreateTable<Contact>();
                Connection.CreateTable<DatabaseVersion>();
            }
        }

        protected override void PopulateDatabase()
        {
            using (Connection)
            using (var generator = new DataGenerator())
            {
                Connection.Insert(generator.GenerateDatabaseVersion());

#if DEBUG
                Connection.InsertAll(generator.GenerateContacts());
#endif
            }
        }

        protected override void UpgradeDatabase()
        {
            // Grabbing the DUWE from the IOC container is a workaround for a circular reference issue
            var duwe = IocWrapper.Get<IDatabaseUpgradeWorkflowExecutor>();
            duwe.ProcessUpgrade();
        }

    }
}
