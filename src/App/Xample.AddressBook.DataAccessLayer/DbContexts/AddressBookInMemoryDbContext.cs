﻿using Xample.AddressBook.DataAccessLayer.Population;
using Xample.Bcl.DataAccess.InMemory;

namespace Xample.AddressBook.DataAccessLayer.DbContexts
{
    public class AddressBookInMemoryDbContext : InMemoryDbContext
    {
        protected override void PopulateDatabase()
        {
            using (var generator = new DataGenerator())
            {
                Insert(generator.GenerateContacts());
            }
        }

    }
}
