﻿namespace Xample.AddressBook.DataAccessLayer.DbContexts
{
    public enum DatabaseImplementation
    {
        InMemory,
        Sqlite
    }
}
