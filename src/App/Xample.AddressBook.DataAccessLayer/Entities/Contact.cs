﻿using SQLite;
using Xample.Bcl.DataAccess;

namespace Xample.AddressBook.DataAccessLayer.Entities
{
    public class Contact : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(200), NotNull]
        public string Name { get; set; }

        [MaxLength(7), NotNull]
        public string ColorHex { get; set; }

        public string ColorName { get; set; }

        public Contact()
        {

        }

        public Contact(int id, string n, string c, string cn)
        {
            Id = id;
            Name = n;
            ColorHex = c;
            ColorName = cn;
        }

    }
}
