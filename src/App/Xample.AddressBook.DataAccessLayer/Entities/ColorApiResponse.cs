﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xample.AddressBook.DataAccessLayer.Entities
{
    public class Hex
    {
        public string Value { get; set; }
        public string Clean { get; set; }
    }

    public class RgbFraction
    {
        public double R { get; set; }
        public double G { get; set; }
        public double B { get; set; }
    }

    public class Rgb
    {
        public RgbFraction Fraction { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public string Value { get; set; }
    }

    public class HslFraction
    {
        public double H { get; set; }
        public double S { get; set; }
        public double L { get; set; }
    }

    public class Hsl
    {
        public HslFraction Fraction { get; set; }
        public int H { get; set; }
        public int S { get; set; }
        public int L { get; set; }
        public string Value { get; set; }
    }

    public class HsvFraction
    {
        public double H { get; set; }
        public double S { get; set; }
        public double V { get; set; }
    }

    public class Hsv
    {
        public HsvFraction Fraction { get; set; }
        public int H { get; set; }
        public int S { get; set; }
        public int V { get; set; }
        public string Value { get; set; }
    }

    public class Name
    {
        public string Value { get; set; }
        public string ClosestNamedHex { get; set; }
        public bool ExactMatchname { get; set; }
        public int Distance { get; set; }
    }

    public class CmykFraction
    {
        public double C { get; set; }
        public double M { get; set; }
        public double Y { get; set; }
        public double K { get; set; }
    }
    public class Cmyk
    {
        public CmykFraction Fraction { get; set; }
        public int C { get; set; }
        public int M { get; set; }
        public int Y { get; set; }
        public int K { get; set; }
        public string Value { get; set; }
    }

    public class XyzFraction
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
    public class Xyz
    {
        public XyzFraction Fraction { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public string Value { get; set; }
    }

    public class Image
    {
        public string Bare { get; set; }
        public string Named { get; set; }
    }

    public class Contrast
    {
        public string Value { get; set; }
    }

    public class Self
    {
        public string Href { get; set; }
    }
    public class Links
    {
        public Self Self { get; set; }
    }

    public class Embedded
    {

    }

    public class ColorApiResponse
    {
        public Hex Hex { get; set; }
        public Rgb Rgb { get; set; }
        public Hsl Hsl { get; set; }
        public Hsv Hsv { get; set; }
        public Name Name { get; set; }
        public Cmyk Cmyk { get; set; }
        public Xyz Xyz { get; set; }
        public Image Image { get; set; }
        public Contrast Contrast { get; set; }
        public Links Links { get; set; }
        public Embedded Embedded { get; set; }
    }
}
