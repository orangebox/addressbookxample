﻿using System;
using System.Threading;
using System.Collections.Generic;
using Xample.AddressBook.DataAccessLayer.Entities;
using Xample.Bcl.Extensions;
using Xample.Bcl.DataAccess.Versioning;

namespace Xample.AddressBook.DataAccessLayer.Population
{
    public class DataGenerator : IDisposable
    {
        #region Variables
        private readonly Dictionary<String, Contact> contactsDictionary;
        private static int uniqueId = 0;
        #endregion

        #region Constructors
        public DataGenerator()
        {
            contactsDictionary = new Dictionary<string, Contact>();
        }
        #endregion

        #region Getters
        private int GetUniqueId()
        {
            return Interlocked.Increment(ref uniqueId);
        }

        private KeyValuePair<String, Contact> GetContact(string name, string colorHex, string colorName)
        {
            var contact = new Contact(GetUniqueId(), name, colorHex, colorName);
            return new KeyValuePair<String, Contact>(name, contact);
        }
        #endregion

        #region Populator Methods
        public IEnumerable<Contact> GenerateContacts()
        {
            contactsDictionary.Add(GetContact("Bruce Banner", "#158202", "Hulk Green"));
            contactsDictionary.Add(GetContact("Cletus Kasady", "#5E1016", "Carnage Red"));
            contactsDictionary.Add(GetContact("Eddie Brock", "#000000", "Venom Black"));
            contactsDictionary.Add(GetContact("Peter Parker", "#ED2939", "Spider Man Red"));

            return contactsDictionary.Values;
        }

        public DatabaseVersion GenerateDatabaseVersion()
        {
            return new DatabaseVersion
            {
                Version = 0
            };
        }
        #endregion

        #region IDisposable Implementation
        ~DataGenerator()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            contactsDictionary.Clear();
        }
        #endregion
    }
}
