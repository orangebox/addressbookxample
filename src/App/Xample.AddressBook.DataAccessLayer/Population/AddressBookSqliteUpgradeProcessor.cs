﻿using System;
using System.Collections.Generic;
using System.Text;
using Xample.Bcl.DataAccess.Versioning;
using Xample.Bcl.DataAccess;
using Xample.AddressBook.DataAccessLayer.Entities;
using Xample.Bcl.Xamarin.PlatformServices;

namespace Xample.AddressBook.DataAccessLayer.Population
{
    public class AddressBookSqliteUpgradeProcessor : IUpgradeProcessor
    {
        private readonly IRepository<Contact> contactRepository;
        private readonly ILogger logger;

        public AddressBookSqliteUpgradeProcessor(IRepository<Contact> cr,
                                                 ILogger l)
        {
            contactRepository = cr;
            logger = l;

            logger.Initialize(GetType());
        }

        public DatabaseVersion HandleUpgrade(DatabaseVersion currentVersion)
        {
            var nextVersion = new DatabaseVersion(currentVersion.Version);
            logger.Trace($"Given database version: ${currentVersion.Version}");
            switch (currentVersion.Version)
            {
                case 0:
                    // This case exists in the event that there are no records in the DatabaseVersion table
                    nextVersion.Version++;
                    goto case 1;
                case 1:
                    //nextVersion.Version++;
                    break;
            }
            logger.Trace($"Upgraded to version {nextVersion.Version}");
            return nextVersion;
        }

    }
}
