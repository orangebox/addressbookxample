﻿using System.Collections.Generic;
using Entities = Xample.AddressBook.DataAccessLayer.Entities;
using UiModels = Xample.AddressBook.Backend.Models;

namespace Xample.AddressBook.Backend.Mappers
{
    public interface IContactMapper
    {
        Entities.Contact Map(UiModels.Contact uiContact);
        UiModels.Contact Map(Entities.Contact contactEntity);

        IEnumerable<Entities.Contact> Map(IEnumerable<UiModels.Contact> uiContacts);
        IEnumerable<UiModels.Contact> Map(IEnumerable<Entities.Contact> contactEntities);
    }

    public class ContactMapper : IContactMapper
    {
        public Entities.Contact Map(UiModels.Contact uiContact)
        {
            return new Entities.Contact
            {
                Id = uiContact.Id,
                Name = uiContact.Name,
                ColorHex = uiContact.ColorHex,
                ColorName = uiContact.ColorName
            };
        }

        public UiModels.Contact Map(Entities.Contact contactEntity)
        {
            return new UiModels.Contact
            {
                Id = contactEntity.Id,
                Name = contactEntity.Name,
                ColorHex = contactEntity.ColorHex,
                ColorName = contactEntity.ColorName
            };
        }

        public IEnumerable<Entities.Contact> Map(IEnumerable<UiModels.Contact> uiContacts)
        {
            foreach (var uiContact in uiContacts)
                yield return Map(uiContact);
        }

        public IEnumerable<UiModels.Contact> Map(IEnumerable<Entities.Contact> contactEntities)
        {
            foreach (var contactEntity in contactEntities)
                yield return Map(contactEntity);
        }

    }
}
