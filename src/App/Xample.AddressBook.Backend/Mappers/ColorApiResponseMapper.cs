﻿using Newtonsoft.Json;
using Xample.AddressBook.DataAccessLayer.Entities;

namespace Xample.AddressBook.Backend.Mappers
{
    public interface IColorApiResponseMapper
    {
        string Map(ColorApiResponse colorApiResponse);
        ColorApiResponse Map(string json);
    }

    public class ColorApiResponseMapper : IColorApiResponseMapper
    {
        public string Map(ColorApiResponse colorApiResponse)
        {
            var json = JsonConvert.SerializeObject(colorApiResponse);
            return json;
        }

        public ColorApiResponse Map(string json)
        {
            var colorApiResponse = JsonConvert.DeserializeObject<ColorApiResponse>(json);
            return colorApiResponse;
        }
    }
}
