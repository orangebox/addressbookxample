﻿using GalaSoft.MvvmLight;

namespace Xample.AddressBook.Backend.Models
{
    public class Contact : ObservableObject
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { Set(nameof(Id), ref id, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { Set(nameof(Name), ref name, value); }
        }

        private string colorHex;
        public string ColorHex
        {
            get { return colorHex; }
            set { Set(nameof(ColorHex), ref colorHex, value); }
        }

        private string colorName;
        public string ColorName
        {
            get { return colorName; }
            set { Set(nameof(ColorName), ref colorName, value); }
        }

        public Contact()
        {
            ColorHex = "#FFFFFF";
        }

        public Contact(int i, string n, string ch, string cn)
        {
            Id = i;
            Name = n;
            ColorHex = ch;
            ColorName = cn;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^
                   Name.GetHashCode() ^
                   ColorHex.GetHashCode() ^
                   ColorName.GetHashCode();
        }

		public override bool Equals(object obj)
		{
            if (obj == null)
                return false;

            var that = obj as Contact;
            if (that == null)
                return false;

            return this.Id == that.Id &&
                   this.Name == that.Name &&
                   this.ColorHex == that.ColorHex &&
                   this.ColorName == that.ColorName;
		}

	}
}
