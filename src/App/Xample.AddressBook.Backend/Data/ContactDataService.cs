﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xample.AddressBook.Backend.Mappers;
using Xample.Bcl.DataAccess;
using Entities = Xample.AddressBook.DataAccessLayer.Entities;
using UiModels = Xample.AddressBook.Backend.Models;

namespace Xample.AddressBook.Backend.Data
{
    public interface IContactDataService
    {
        // create/update
        UiModels.Contact PersistChanges(UiModels.Contact contact);

        // get
        IEnumerable<UiModels.Contact> GetAllContacts();
		UiModels.Contact GetContact(int id);

        // search
        IEnumerable<UiModels.Contact> SearchByName(string searchCriteria);
    }

    public class ContactDataService : IContactDataService
    {
        #region Variables
        private readonly IRepository<Entities.Contact> contactRepository;
        private readonly IContactMapper contactMapper;
        #endregion

        #region Constructors
        public ContactDataService(IRepository<Entities.Contact> cr, IContactMapper cm)
        {
            contactRepository = cr;
            contactMapper = cm;
        }
        #endregion

        #region Create/Update
        public UiModels.Contact PersistChanges(UiModels.Contact contact)
        {
            if (contact.Id == 0)
                return SaveContact(contact);
            else
                return UpdateContact(contact);
        }

        private UiModels.Contact SaveContact(UiModels.Contact contact)
        {
            contact.Name = contact.Name.Trim();
            var contactEntity = contactMapper.Map(contact);

            contactRepository.Add(contactEntity);

            var uiContact = contactMapper.Map(contactEntity);
            return uiContact;
        }

        private UiModels.Contact UpdateContact(UiModels.Contact contact)
        {
            contact.Name = contact.Name.Trim();
            var contactEntity = contactMapper.Map(contact);

            contactRepository.Update(contactEntity);

            var uiContact = contactMapper.Map(contactEntity);
            return uiContact;
        }
        #endregion

        #region Get
        public IEnumerable<UiModels.Contact> GetAllContacts()
        {
            var contacts = contactRepository.GetAll().OrderBy(c => c.Name).ThenBy(c => c.Id);
            return contactMapper.Map(contacts);
        }
		
        public UiModels.Contact GetContact(int contactId)
        {
            if (contactId > 0)
            {
                var contactEntity = contactRepository.Get(contactId);
                return contactMapper.Map(contactEntity);
            }
            else
                return new UiModels.Contact();
        }
        #endregion

        #region Search
        private static bool IsTextMatch(Entities.Contact contact, string searchCriteria)
        {
            return contact.Name.ToLower().Contains(searchCriteria);
        }

        public IEnumerable<UiModels.Contact> SearchByName(string searchCriteria)
        {
            if (String.IsNullOrEmpty(searchCriteria))
                return GetAllContacts();

            searchCriteria = searchCriteria.ToLower();
            var contactEntities = contactRepository.Find(c => IsTextMatch(c, searchCriteria)).OrderBy(c => c.Name).ThenBy(c => c.Id);

            var uiContacts = contactMapper.Map(contactEntities);
            return uiContacts.OrderBy(c => c.Name);
        }
        #endregion
    }
}
