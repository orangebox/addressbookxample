﻿using System;
using System.Threading.Tasks;
using Xample.AddressBook.Backend.Mappers;
using Xample.Bcl.DataAccess.Rest;

namespace Xample.AddressBook.Backend.Data
{
    public interface IColorDataService
    {
        Task<String> GetColorName(string colorHex);
    }

    public class ColorDataService : IColorDataService
    {
        private const string GET_URL_FORMAT = "http://thecolorapi.com/id?hex={0}&format=json";
        private readonly IRestContext restContext;
        private readonly IColorApiResponseMapper colorApiResponseMapper;

        public ColorDataService(IRestContext rr, IColorApiResponseMapper carm)
        {
            restContext = rr;
            colorApiResponseMapper = carm;
        }

        public async Task<String> GetColorName(string colorHex)
        {
            colorHex = colorHex.Replace("#", String.Empty);
            var path = String.Format(GET_URL_FORMAT, colorHex);
            var jsonResponse = await restContext.GetStringAsync(path);

            var colorApiResponse = colorApiResponseMapper.Map(jsonResponse);

            return colorApiResponse.Name.Value;
        }

    }
}
