﻿using System;
using Xample.AddressBook.Backend.Models;
using System.Text.RegularExpressions;

namespace Xample.AddressBook.Backend.Validators
{
    public interface IContactValidator
    {
        bool HasRequiredFields(Contact contact);
    }

    public class ContactValidator : IContactValidator
    {
        private bool ValidateContactName(string name)
        {
            return !String.IsNullOrEmpty(name) &&
                   name.Length <= 200;
        }

        private bool ValidateContactColorHex(string hex)
        {
            return !String.IsNullOrEmpty(hex) &&
                   Regex.Match(hex, "#[A-Fa-f0-9]{6}").Success;
        }

        public bool HasRequiredFields(Contact contact)
        {
            return ValidateContactName(contact.Name) &&
                   ValidateContactColorHex(contact.ColorHex);
        }

    }
}
