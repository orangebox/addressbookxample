﻿using System;

namespace Xample.AddressBook.Backend.Business
{
    public interface IRandomColorSelector
    {
        string GetRandomColorHex();
    }

    public class RandomColorSelector : IRandomColorSelector
    {
        private readonly string[] HEX_CHARACTERS = 
        {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "A", "B", "C", "D", "E", "F"
        };

        public string GetRandomColorHex()
        {
            var generator = new Random();
            var color = String.Empty;

            for (var index = 0; index < 6; index++)
            {
                var randomIndex = generator.Next(HEX_CHARACTERS.Length);
                color += HEX_CHARACTERS[randomIndex];
            }

            return $"#{color}";
        }

    }
}
