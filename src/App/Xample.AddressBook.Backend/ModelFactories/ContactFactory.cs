﻿using System.Collections.Generic;
using Entities = Xample.AddressBook.DataAccessLayer.Entities;
using UiModels = Xample.AddressBook.Backend.Models;

namespace Xample.AddressBook.Backend.ModelFactories
{
    public static class ContactFactory
    {
        private static readonly string[] colors = new string[]
        {
            "#158202",
            "#5E1016",
            "#000000",
            "#ED2939"
        };

        public static Entities.Contact GetContactEntity(int id)
        {
            var colorIndex = id % colors.Length;

            return new Entities.Contact
            {
                Id = id,
                Name = $"Contact Name{id}",
                ColorHex = colors[colorIndex],
                ColorName = $"Sample {colorIndex}"
            };
        }

        public static IEnumerable<Entities.Contact> GetContactEntities(int count)
        {
            for (var index = 0; index < count; index++)
                yield return GetContactEntity(index + 1);
        }

        public static UiModels.Contact GetUiContact(int id)
        {
            var colorIndex = id % colors.Length;

            return new UiModels.Contact
            {
                Id = id,
                Name = $"Contact Name{id}",
                ColorHex = colors[colorIndex],
                ColorName = $"Sample {colorIndex}"
            };
        }

        public static IEnumerable<UiModels.Contact> GetUiContacts(int count)
        {
            for (var index = 0; index < count; index++)
                yield return GetUiContact(index + 1);
        }

    }
}
