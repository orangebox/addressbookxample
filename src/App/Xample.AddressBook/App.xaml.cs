﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xample.AddressBook.Initialization;
using Xample.Bcl.Extensions;

namespace Xample.AddressBook
{
    public partial class App : Application
    {
        public static bool DesignMode { get; set; } = true;
        public static App Instance { get { return Application.Current as App; } }

        public App()
            : this(true)
        {
            DesignBootstrapper.ExecuteSteps();
        }

        public App(bool dgnMode)
        {
            DesignMode = dgnMode;
            InitializeComponent();

            TaskScheduler.UnobservedTaskException += (s, e) =>
            {
                e.SetObserved();
                OnUnhandledException(e.Exception);
            };
        }

        protected override void OnStart()
        {
            RuntimeBootstrapper.ExecuteSteps();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public void OnUnhandledException(Exception ex)
        {
            var exceptions = ex.Flatten();
            exceptions.PrintToConsole();

            // TODO send the exception to MS app center
        }
    }
}
