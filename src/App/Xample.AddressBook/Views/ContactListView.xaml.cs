﻿using System;
using Xamarin.Forms;
using Xample.AddressBook.ViewModels;
using Xample.Bcl.Xamarin.Runtime;

namespace Xample.AddressBook.Views
{
    public partial class ContactListView : ContentPage
    {
        private readonly ContactListViewModel viewModel;

        public ContactListView()
        {
            InitializeComponent();
            viewModel = BindingContext as ContactListViewModel;

            if (ExecutionContext.InDesignMode)
                return;

            Appearing += OnLoad;
            Disappearing += OnUnload;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            viewModel.LoadedCommand.Execute(null);
        }

        private void OnUnload(object sender, EventArgs e)
        {
            viewModel.UnloadedCommand.Execute(null);
        }
    }
}
