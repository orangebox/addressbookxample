﻿using System;
using Xamarin.Forms;
using Xample.AddressBook.ViewModels;
using Xample.Bcl.Xamarin.Runtime;

namespace Xample.AddressBook.Views
{
    public partial class ContactDetailsView : ContentPage
    {
        private readonly ContactDetailsViewModel viewModel;
        private readonly int contactId;

        public ContactDetailsView()
            : this(0)
        {

        }

        public ContactDetailsView(int contactId)
        {
            InitializeComponent();

            this.contactId = contactId;
            viewModel = BindingContext as ContactDetailsViewModel;

            if (ExecutionContext.InDesignMode)
                return;

            Appearing += OnLoad;
            Disappearing += OnUnload;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            viewModel.LoadedCommand.Execute(contactId);
        }

        private void OnUnload(object sender, EventArgs e)
        {
            viewModel.UnloadedCommand.Execute(null);
        }

    }
}
