﻿using System;
using Xample.AddressBook.ViewModels;
using Xample.Bcl.Xamarin.Mvvm.Navigation;
using Xamarin.Forms;
using Xample.Bcl.Xamarin.Runtime;

namespace Xample.AddressBook.Views
{
    public partial class EditContactView : ModalPage
    {
        private readonly EditContactViewModel viewModel;
        private readonly int contactId;

        public EditContactView()
            : this(0)
        {

        }

        public EditContactView(int contactId)
        {
            InitializeComponent();

            this.contactId = contactId;
            viewModel = BindingContext as EditContactViewModel;

            if (ExecutionContext.InDesignMode)
                return;

            Appearing += OnLoad;
            Disappearing += OnUnload;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            viewModel.LoadedCommand.Execute(contactId);
        }

        private void OnUnload(object sender, EventArgs e)
        {
            viewModel.UnloadedCommand.Execute(null);
        }
    }
}
