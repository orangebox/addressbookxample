﻿using Xample.AddressBook.Backend.ModelFactories;
using Xample.AddressBook.ViewModels;

namespace Xample.AddressBook.DesignViewModels
{
    public class DesignContactDetailsViewModel : ContactDetailsViewModel
    {
        public DesignContactDetailsViewModel()
            : base(null, null, null)
        {
            var generatedContact = ContactFactory.GetUiContact(5);
            Contact = generatedContact;
        }
    }
}
