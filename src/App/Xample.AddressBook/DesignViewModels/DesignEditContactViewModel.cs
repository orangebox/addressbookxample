﻿using Xample.AddressBook.Backend.ModelFactories;
using Xample.AddressBook.ViewModels;

namespace Xample.AddressBook.DesignViewModels
{
    public class DesignEditContactViewModel : EditContactViewModel
    {
        public DesignEditContactViewModel()
            : base(null, null, null, null, null, null)
        {
            var generatedContact = ContactFactory.GetUiContact(5);
            Contact = generatedContact;
        }

    }
}
