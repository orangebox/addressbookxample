﻿using System.Collections.ObjectModel;
using Xample.AddressBook.Backend.ModelFactories;
using Xample.AddressBook.Backend.Models;
using Xample.AddressBook.ViewModels;

namespace Xample.AddressBook.DesignViewModels
{
    public class DesignContactListViewModel : ContactListViewModel
    {
        public DesignContactListViewModel()
            : base(null, null, null)
        {
            var generatedContacts = ContactFactory.GetUiContacts(10);
            Contacts = new ObservableCollection<Contact>(generatedContacts);
        }

    }
}
