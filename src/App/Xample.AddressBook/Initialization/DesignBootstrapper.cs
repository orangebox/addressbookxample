﻿using Xample.Bcl.Ioc;
using Xample.Bcl.Xamarin.PlatformServices;

namespace Xample.AddressBook.Initialization
{
    public static class DesignBootstrapper
    {
        public static void ExecuteSteps()
        {
            IocWrapper.Register<ILogger, DesignLogger>();
            IocRegistrar.RegisterDesignViewModels();
        }

    }
}
