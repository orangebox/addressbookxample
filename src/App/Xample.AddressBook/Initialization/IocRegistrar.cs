﻿using System;
using Xample.Bcl.Ioc;
using Xample.AddressBook.ViewModels;
using Xample.AddressBook.DesignViewModels;
using Xample.Bcl.Xamarin.Mvvm.Dialog;
using Xample.Bcl.Xamarin.Mvvm.Navigation;
using Xample.Bcl.DataAccess.InMemory;
using Xample.AddressBook.DataAccessLayer.DbContexts;
using Xample.Bcl.DataAccess;
using Xample.AddressBook.DataAccessLayer.Entities;
using Xample.Bcl.DataAccess.Versioning;
using Xample.AddressBook.DataAccessLayer.Population;
using Xample.Bcl.DataAccess.Sqlite;
using Xample.Bcl.Xamarin.PlatformServices;
using Xample.AddressBook.Backend.Mappers;
using Xample.AddressBook.Backend.Data;
using Xample.AddressBook.Backend.Business;
using Xample.AddressBook.Backend.Validators;
using Xample.Bcl.DataAccess.Restful;

namespace Xample.AddressBook.Initialization
{
    public static class IocRegistrar
    {
        public static void RegisterDesignViewModels()
        {
            IocWrapper.Register<ContactDetailsViewModel, DesignContactDetailsViewModel>();
            IocWrapper.Register<ContactListViewModel, DesignContactListViewModel>();
            IocWrapper.Register<EditContactViewModel, DesignEditContactViewModel>();
        }

        public static void RegisterViewModels()
        {
            IocWrapper.Register<ContactDetailsViewModel>();
            IocWrapper.Register<ContactListViewModel>();
            IocWrapper.Register<EditContactViewModel>();
        }

        public static void RegisterDialogService()
        {
            IocWrapper.Register<IDialogService, DialogService>();
        }

        public static void RegisterNavigationService()
        {
            IocWrapper.Register<INavigationService>(() =>
            {
                var ns = new NavigationService();

                ns.Configure(typeof(ContactDetailsViewModel), typeof(Views.ContactDetailsView));
                ns.Configure(typeof(ContactListViewModel), typeof(Views.ContactListView));
                ns.Configure(typeof(EditContactViewModel), typeof(Views.EditContactView));

                return ns;
            });
        }

        public static void RegisterInMemoryRepositories()
        {
            IocWrapper.Register<InMemoryDbContext, AddressBookInMemoryDbContext>();
            IocWrapper.Register<IRepository<Contact>, InMemoryRepository<Contact>>();
            IocWrapper.Register<IRepository<DatabaseVersion>, InMemoryRepository<DatabaseVersion>>();
        }

        public static void RegisterSqliteRepositories()
        {
            IocWrapper.Register<IDatabaseUpgradeWorkflowExecutor, DatabaseUpgradeWorkflowExecutor>();
            IocWrapper.Register<IUpgradeProcessor, AddressBookSqliteUpgradeProcessor>();

            IocWrapper.Register<SqliteDbContext>(() =>
            {
                var dbFilePath = IocWrapper.Get<IFileHelper>().GetLocalFilePath("AddressBookSqlite.db3");
                var dbc = new AddressBookSqliteDbContext(dbFilePath);
                return dbc;
            });

            IocWrapper.Register<IRepository<Contact>, SqliteRepository<Contact>>();
            IocWrapper.Register<IRepository<DatabaseVersion>, SqliteRepository<DatabaseVersion>>();
        }

        public static void RegisterRestfulRepositories()
        {
            IocWrapper.Register<IRestContext, RestContext>();
        }

        public static void RegisterMappers()
        {
            IocWrapper.Register<IContactMapper, ContactMapper>();
            IocWrapper.Register<IColorApiResponseMapper, ColorApiResponseMapper>();
        }

        public static void RegisterDataServices()
        {
            IocWrapper.Register<IContactDataService, ContactDataService>();
            IocWrapper.Register<IColorDataService, ColorDataService>();
            IocWrapper.Register<IDatabaseVersionDataService, DatabaseVersionDataService>();
        }

        public static void RegisterBusinessLayer()
        {
            IocWrapper.Register<IRandomColorSelector, RandomColorSelector>();
        }

        public static void RegisterValidators()
        {
            IocWrapper.Register<IContactValidator, ContactValidator>();
        }

    }
}
