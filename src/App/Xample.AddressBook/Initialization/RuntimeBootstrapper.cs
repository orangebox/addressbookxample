﻿using System;
using Xamarin.Forms;
using Xample.AddressBook.DataAccessLayer.DbContexts;
using Xample.AddressBook.Views;
using Xample.Bcl.DataAccess;
using Xample.Bcl.DataAccess.InMemory;
using Xample.Bcl.DataAccess.Sqlite;
using Xample.Bcl.Ioc;
using Xample.Bcl.Xamarin.Mvvm.Dialog;
using Xample.Bcl.Xamarin.Mvvm.Navigation;

namespace Xample.AddressBook.Initialization
{
    public static class RuntimeBootstrapper
    {
        private const DatabaseImplementation databaseImplementation = DatabaseImplementation.Sqlite;

        public static void ExecuteSteps()
        {
            InitializeDependencyInjection();
            InitializeDatabase();
            SetInitialView();
        }

        private static void InitializeDependencyInjection()
        {
            IocRegistrar.RegisterDialogService();
            IocRegistrar.RegisterNavigationService();

            IocRegistrar.RegisterRestfulRepositories();
            IocRegistrar.RegisterDataServices();

            switch (databaseImplementation)
            {
                case DatabaseImplementation.InMemory:
                    IocRegistrar.RegisterInMemoryRepositories();
                    break;
                case DatabaseImplementation.Sqlite:
                    IocRegistrar.RegisterSqliteRepositories();
                    break;
            }

            IocRegistrar.RegisterMappers();
            IocRegistrar.RegisterBusinessLayer();
            IocRegistrar.RegisterValidators();
            IocRegistrar.RegisterViewModels();
        }

        private static void InitializeDatabase()
        {
            DbContextBase dbContext;

            //var databaseImplementation = IocWrapper.Get<ISettingsDataService>().DatabaseImplementation;
            switch (databaseImplementation)
            {
                case DatabaseImplementation.Sqlite:
                    dbContext = IocWrapper.Get<SqliteDbContext, DbContextBase>();
                    break;
                case DatabaseImplementation.InMemory:
                    dbContext = IocWrapper.Get<InMemoryDbContext, DbContextBase>();
                    break;
                default:
                    throw new NotSupportedException($"Unhandled database implementation: {databaseImplementation}");
            }

            dbContext.InitDb();
        }

        private static void SetInitialView()
        {
            var firstPage = new NavigationPage(new ContactListView());

            InitializeDialogService(firstPage);
            InitializeNavigationService(firstPage);

            App.Instance.MainPage = firstPage;
        }

        private static void InitializeDialogService(Page rootPage)
        {
            var dialogService = IocWrapper.Get<IDialogService>();
            (dialogService as DialogService).Initialize(rootPage);
        }

        private static void InitializeNavigationService(NavigationPage rootNavPage)
        {
            var navigationService = IocWrapper.Get<INavigationService>();
            (navigationService as NavigationService).Initialize(rootNavPage);
        }

    }
}
