﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Xample.AddressBook.Backend.Data;
using Xample.AddressBook.Backend.Models;
using Xample.Bcl.Xamarin.Mvvm.Dialog;
using Xample.Bcl.Xamarin.Mvvm.Navigation;

namespace Xample.AddressBook.ViewModels
{
    public class ContactListViewModel : ViewModelBase
    {
        #region Variables
        private readonly IContactDataService contactDataService;
        private readonly INavigationService navigationService;
        private readonly IDialogService dialogService;
        #endregion

        #region Binding Variables
        private ObservableCollection<Contact> contacts;
        public ObservableCollection<Contact> Contacts
        {
            get { return contacts; }
            set { Set(nameof(Contacts), ref contacts, value); }
        }
        #endregion

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand UnloadedCommand { get; private set; }
        public RelayCommand<Contact> ViewContactCommand { get; private set; }
        public RelayCommand<Contact> EditContactCommand { get; private set; }
        public RelayCommand AddNewContactCommand { get; private set; }
        #endregion

        #region Constructors
        public ContactListViewModel(IContactDataService cds,
                                    INavigationService n,
                                    IDialogService d)
        {
            contactDataService = cds;
            navigationService = n;
            dialogService = d;

            InitializeProperties();

            LoadedCommand = new RelayCommand(LoadedExecuted);
            UnloadedCommand = new RelayCommand(UnloadedExecuted);
            ViewContactCommand = new RelayCommand<Contact>(ViewContactExecuted);
            EditContactCommand = new RelayCommand<Contact>(EditContactExecuted);
            AddNewContactCommand = new RelayCommand(AddNewContactExecuted);
        }
		#endregion

        #region Private Methods
        private void InitializeProperties()
        {
            Contacts = new ObservableCollection<Contact>();
        }
        #endregion

        #region Commands Executed
        private void LoadedExecuted()
        {
            var c = contactDataService.GetAllContacts();
            Contacts = new ObservableCollection<Contact>(c);
        }

        private void UnloadedExecuted()
        {
            InitializeProperties();
        }

        private void ViewContactExecuted(Contact contact)
        {
            var contactId = contact.Id;
            navigationService.NavigateTo(typeof(ContactDetailsViewModel).Name, contactId);
        }

        private void EditContactExecuted(Contact contact)
        {
            var contactId = contact.Id;
            navigationService.NavigateTo(typeof(EditContactViewModel).Name, contactId);
        }

        private void AddNewContactExecuted()
        {
            navigationService.NavigateTo(typeof(EditContactViewModel).Name, 0);
        }
        #endregion

    }
}
