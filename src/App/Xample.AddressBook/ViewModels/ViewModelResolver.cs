﻿using System.Linq;
using System.Reflection;
using Xample.Bcl.Ioc;

namespace Xample.AddressBook.ViewModels
{
    public class ViewModelResolver
    {
        public object Resolve(string viewModelName)
        {
            var vmtype = this.GetType()
                             .GetTypeInfo()
                             .Assembly
                             .DefinedTypes
                             .FirstOrDefault(t => t.Name.Equals(viewModelName))
                             .AsType();

            return IocWrapper.Get(vmtype);
        }

    }
}

