﻿using System;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Xample.AddressBook.Backend.Business;
using Xample.AddressBook.Backend.Data;
using Xample.AddressBook.Backend.Models;
using Xample.AddressBook.Backend.Validators;
using Xample.Bcl.Xamarin.Mvvm.Dialog;
using Xample.Bcl.Xamarin.Mvvm.Navigation;

namespace Xample.AddressBook.ViewModels
{
    public class EditContactViewModel : ViewModelBase
    {
        #region Variables
        private readonly IContactDataService contactDataService;
        private readonly IContactValidator contactValidator;
        private readonly IRandomColorSelector randomColorSelector;
        private readonly IColorDataService colorDataService;
        private readonly INavigationService navigationService;
        private readonly IDialogService dialogService;
        #endregion

        #region Binding Variables
        private Contact contact;
        public Contact Contact
        {
            get { return contact; }
            set { Set(nameof(Contact), ref contact, value); }
        }
        #endregion

        #region Commands
        public RelayCommand<Int32> LoadedCommand { get; private set; }
        public RelayCommand UnloadedCommand { get; private set; }
        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }
        #endregion

        #region Constructors
        public EditContactViewModel(IContactDataService cds,
                                    IContactValidator cv,
                                    IRandomColorSelector rcs,
                                    IColorDataService colorDs,
                                    INavigationService n,
                                    IDialogService dlg)
        {
            contactDataService = cds;
            contactValidator = cv;
            randomColorSelector = rcs;
            colorDataService = colorDs;
            navigationService = n;
            dialogService = dlg;

            InitializeProperties();

            LoadedCommand = new RelayCommand<Int32>(LoadedExecuted);
            UnloadedCommand = new RelayCommand(UnloadedExecuted);
            SaveCommand = new RelayCommand(SaveExecuted, SaveCanExecute);
            CancelCommand = new RelayCommand(CancelExecuted);
        }
        #endregion

        #region Private Methods
        private void InitializeProperties()
        {
            Contact = new Contact();
        }
        #endregion

        #region Commands CanExecute
        private bool SaveCanExecute()
        {
            if (contactValidator == null)
                return true;

            return contactValidator.HasRequiredFields(Contact);
        }

        private void OnContactPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SaveCommand.RaiseCanExecuteChanged();
        }
        #endregion

        #region Commands Executed
        private async void LoadedExecuted(int id)
        {
            Contact = contactDataService.GetContact(id);
            if (id == 0)
            {
                var randomColor = randomColorSelector.GetRandomColorHex();
                var colorName = await colorDataService.GetColorName(randomColor);
                Contact.ColorHex = randomColor;
                Contact.ColorName = colorName;
            }

            Contact.PropertyChanged += OnContactPropertyChanged;
        }

        private void UnloadedExecuted()
        {
            Contact.PropertyChanged -= OnContactPropertyChanged;
            InitializeProperties();
        }

        private async void SaveExecuted()
        {
            try
            {
                var savedContact = contactDataService.PersistChanges(Contact);
                await dialogService.ShowMessage($"Saved {savedContact.Name} with ID {savedContact.Id}.", 
                                                "Saved", 
                                                "OK", 
                                                navigationService.GoBack);
            }
            catch (Exception ex)
            {
                await dialogService.ShowError(ex, "Unable to save", "OK");
            }
        }
		
        private void CancelExecuted()
        {
            navigationService.GoBack();
        }
        #endregion

    }
}
