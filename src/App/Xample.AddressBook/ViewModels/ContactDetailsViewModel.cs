﻿using System;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Xample.AddressBook.Backend.Data;
using Xample.AddressBook.Backend.Models;
using Xample.Bcl.Xamarin.Mvvm.Dialog;
using Xample.Bcl.Xamarin.Mvvm.Navigation;

namespace Xample.AddressBook.ViewModels
{
    public class ContactDetailsViewModel : ViewModelBase
    {
        #region Variables
        private readonly IContactDataService contactDataService;
        private readonly INavigationService navigationService;
        private readonly IDialogService dialogService;
        #endregion

        #region Binding Variables
        private Contact contact;
        public Contact Contact
        {
            get { return contact; }
            set { Set(nameof(Contact), ref contact, value); }
        }
        #endregion

        #region Commands
        public RelayCommand<Int32> LoadedCommand { get; private set; }
        public RelayCommand UnloadedCommand { get; private set; }
        public RelayCommand<Int32> EditContactCommand { get; private set; }
        public RelayCommand GoBackCommand { get; private set; }
        #endregion

        #region Constructors
        public ContactDetailsViewModel(IContactDataService cds, 
                                       INavigationService n, 
                                       IDialogService dlg)
        {
            contactDataService = cds;
            navigationService = n;
            dialogService = dlg;

            InitializeProperties();

            LoadedCommand = new RelayCommand<Int32>(LoadedExecuted);
            UnloadedCommand = new RelayCommand(UnloadedExecuted);
            EditContactCommand = new RelayCommand<Int32>(EditContactExecuted);
            GoBackCommand = new RelayCommand(GoBackExecuted);
        }
		#endregion

        #region Private Methods
        private void InitializeProperties()
        {
            Contact = new Contact();
        }
        #endregion

        #region Commands Executed
        private void LoadedExecuted(int id)
        {
            Contact = contactDataService.GetContact(id);
        }

        private void UnloadedExecuted()
        {
            InitializeProperties();
        }

        private void EditContactExecuted(int contactId)
        {
            navigationService.NavigateTo(typeof(EditContactViewModel).Name, contactId);
        }

        private void GoBackExecuted()
        {
            navigationService.GoBack();
        }
        #endregion

    }
}
