﻿using System.Dynamic;
using Xample.Bcl.Ioc;
using Xample.Bcl.Xamarin.Mvvm.Dialog;
using Xample.Bcl.Xamarin.Mvvm.Navigation;

namespace Xample.AddressBook.ViewModels
{
    public class ViewModelLocator : DynamicObject
    {
        private static ViewModelResolver _resolver;
        public static ViewModelResolver Resolver { get { return _resolver ?? (_resolver = new ViewModelResolver()); } }
        public object this[string viewModelName] { get { return Resolver.Resolve(viewModelName); } }

        public IDialogService DialogService { get { return IocWrapper.Get<IDialogService>(); } }
        public INavigationService NavigationService { get { return IocWrapper.Get<INavigationService>(); } }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = this[binder.Name];
            return true;
        }

    }
}
