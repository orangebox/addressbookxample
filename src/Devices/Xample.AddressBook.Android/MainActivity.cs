﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xample.AddressBook.Android.Initialization;
using Xample.Bcl.Xamarin.Runtime;

namespace Xample.AddressBook.Android
{
    [Activity(Label = "Xample.AddressBook", 
              Icon = "@drawable/icon", 
              Theme = "@style/MainTheme", 
              MainLauncher = true, 
              ConfigurationChanges = 
              ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            ExecutionContext.InDesignMode = false;
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Forms.Init(this, bundle);

            var xamarinApp = Bootstrapper.ExecuteSteps();
            LoadApplication(xamarinApp);
        }

    }
}

