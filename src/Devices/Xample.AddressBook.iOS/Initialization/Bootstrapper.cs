﻿using System;
using Xample.Bcl.Ioc;
using Xample.Bcl.iOS.PlatformServices;
using Xample.Bcl.Xamarin.PlatformServices;
using Xample.Bcl.Xamarin.Runtime;

namespace Xample.AddressBook.iOS.Initialization
{
    public static class Bootstrapper
    {
        public static App ExecuteSteps()
        {
            var app = CreateApp(ExecutionContext.InDesignMode);
            RegisterPlatformServices();
            InitializeUnhandledExceptionEvent(app.OnUnhandledException);
            return app;
        }

        private static App CreateApp(bool designMode)
        {
            var app = new App(designMode);
            return app;
        }

        private static void RegisterPlatformServices()
        {
            IocWrapper.Register<IFileHelper, FileHelper>();
            IocWrapper.Register<ILogger, Logger>();
        }

        private static void InitializeUnhandledExceptionEvent(Action<Exception> onUnhandledException)
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                onUnhandledException(e.ExceptionObject as Exception);
            };
        }

    }
}
