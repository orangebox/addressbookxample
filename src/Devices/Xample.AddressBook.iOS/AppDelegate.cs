﻿using Foundation;
using UIKit;
using Xamarin.Forms;
using Xample.Bcl.Xamarin.Runtime;
using Xample.AddressBook.iOS.Initialization;

namespace Xample.AddressBook.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication uiApplication, NSDictionary launchOptions)
        {
            ExecutionContext.InDesignMode = false;
            Forms.Init();

            var xamarinApp = Bootstrapper.ExecuteSteps();
            LoadApplication(xamarinApp);

            return base.FinishedLaunching(uiApplication, launchOptions);
        }

    }
}
