﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Xample.Bcl.DataAccess.Rest;

namespace Xample.Bcl.Tests.DataAccess.Restful
{
    [TestFixture]
    public class RestContextTests
    {
        // Variables
        private const string BASE_URL_FORMAT = "https://jsonplaceholder.typicode.com/{0}";
        private IRestContext restContext;

        [SetUp]
        public void BeforeEach()
        {
            restContext = new RestContext();
        }

        // GetStringAsync tests
        [Test]
        public async Task GetStringAsyncShouldReturnJson()
        {
            var url = String.Format(BASE_URL_FORMAT, "posts/1");

            var json = await restContext.GetStringAsync(url);

            json.Should().NotBeNullOrEmpty();
        }

        // GetAsync tests
        [Test]
        public async Task GetAsyncShouldReturnResponse()
        {
            var url = String.Format(BASE_URL_FORMAT, "posts/1");

            var response = await restContext.GetAsync(url);

            response.Should().NotBeNull();
            var jsonResponse = await response.Content.ReadAsStringAsync();
            jsonResponse.Should().NotBeNullOrEmpty();

            Console.WriteLine($"JSON response: '{jsonResponse}'");
        }

        // PostAsync tests
        [Test]
        public async Task PostAsyncShouldReturnResponse()
        {
            var url = String.Format(BASE_URL_FORMAT, "posts");
            var json = "{ title: 'foo', body: 'bar', userId: 1 }";

            var response = await restContext.PostAsync(url, json);

            response.Should().NotBeNull();
            var jsonResponse = await response.Content.ReadAsStringAsync();
            jsonResponse.Should().NotBeNullOrEmpty();

            Console.WriteLine($"JSON request: '{json}'");
            Console.WriteLine($"JSON response: '{jsonResponse}'");
        }

        // PutAsync tests
        [Test]
        public async Task PutAsyncShouldReturnResponse()
        {
            var url = String.Format(BASE_URL_FORMAT, "posts/1");
            var json = "{ title: 'foo', body: 'bar', userId: 1 }";

            var response = await restContext.PutAsync(url, json);

            response.Should().NotBeNull();
            var jsonResponse = await response.Content.ReadAsStringAsync();
            jsonResponse.Should().NotBeNullOrEmpty();

            Console.WriteLine($"JSON request: '{json}'");
            Console.WriteLine($"JSON response: '{jsonResponse}'");
        }

        // DeleteAsync tests
        [Test]
        public async Task DeleteAsyncShouldReturnResponse()
        {
            var url = String.Format(BASE_URL_FORMAT, "posts/1");

            var response = await restContext.DeleteAsync(url);

            response.Should().NotBeNull();
            var jsonResponse = await response.Content.ReadAsStringAsync();
            jsonResponse.Should().NotBeNullOrEmpty();

            Console.WriteLine($"JSON response: '{jsonResponse}'");
        }

    }
}
